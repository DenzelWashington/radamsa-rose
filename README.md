# Radamsa Rose
<small>Version 0.9.6</small>
<small>Authored by Rias Hilliard, Benjamin Hall, Weite Li, and Chelsey Yin</small>

A library adaptation of Aki Helin's radamsa fuzzer.

## Contents
- [Radamsa Rose](#radamsa-rose)
	- [Contents](#contents)
	- [Overview](#overview)
	- [Installation](#installation)
	- [Usage](#usage)
		- [Constants](#constants)
		- [Running the Fuzzer](#running-the-fuzzer)
		- [Customizing the Fuzzer](#customizing-the-fuzzer)
		- [Generators](#generators)
		- [Patterns](#patterns)
		- [Mutations](#mutations)
		- [Additions](#additions)
	- [Comparison to Radamsa](#comparison-to-radamsa)
		- [Running N times](#running-n-times)
	- [API](#api)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## Overview
Radamsa Rose is a C shared library that adapts the functionality of one radamsa call. It has the added functionality of being able to change the constants that radamsa uses to run. The constants and their use are documented below.

## Installation
This installation guide will showcase how to install the library and use CMake to include and use the library. These steps will walk through the process of building the library.

This process requires: _CMake 3.10_ or higher and a _c/c++ compiler_

1. Clone the repository using `git clone https://gitlab.com/hallbh/radamsa-rose.git`
2. Navigate into the directory using `cd radamsa-rose`
3. From there make and navigate into a build folder with the call `mkdir build && cd build`
4. Run cmake to prepare the library build `cmake ..`. There are two options BUILD_RR_TEST, for building the test suite, and BUILD_RADAMSA_ROSE_APP for building the application version of the software.
5. Run `make` to build the library (tests and app if the options are turned to ON)
6. Running `make install` will install the library and headers to the default locations.

Once the library has been install, to use it with CMake, add the following code to your CMakeLists.txt:
```cmake
find_package(radamsa-rose REQUIRED)
target_link_libraries(myapp PRIVATE Rose::radamsa-rose)
```
Note where files are installed. This installation is not perfect, and might be subject to change.

## Usage
Once the shared library is linked to the project, it is very straightforward to use. Below is an example usage in its simplest form:

```c
#include "radamsa-rose/rose-fuzzer.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Example usage: echo "*input*" | ./rad_rose seed
int main(int argc, const char* argv[]) {
    struct radamsa_constants constants = { 0 };
    initDefaultConstants(&constants);

    int seed = atoi(argv[1]);
    radamsa_rose fuzzer = newRadamsaRoseFuzzer(seed, constants);

    char out[1024];
    char in[1024];
    fgets(in, 1024, stdin);

    size_t len = runRadamsa(fuzzer, in, strlen(in), out, 1024);
    out[len] = '\0';
    printf("%s\n", out);

    deleteRadamsaRoseFuzzer(fuzzer);

    return 0;
}
```

To use the library, include it with `#include "radamsa-rose/rose-fuzzer.h"`

### Constants
The constants are created to start. The constants are as follows:

| Name | Default Value | Purpose |
|------|---------------|---------|
| int maxHashCount | 1,048,576 | The size of the hash tree for checksums (Not used by our library) |
| int searchFuel | 100,000 | Node searching in fuse-this, fuse-old, fuse-next, and jumpgen. Number of possible fuses increases directly as this number gets larger |
| int searchStopIp | 8 | Inverse probability to stop fusing in the fuse mutation. Essentially, the 1/8 possibility of stopping in fuse before the search fuel is exhausted. |
| int storedElements | 10 | Textual lines that can be carried across byte chunks with lis and lrs mutations. The greater this number, the greater variance in the textual lines that could be inserted somewhere else. |
| int updateProbability | 20 | Chance of replacing one of the stored textual lines (works directly with storedElements) |
| int minScore | 2 | The minimum score of all mutations. Affects the probability of a mutation being run. |
| int maxScore | 10 | The maximum score of all mutations. Affects the probability of a mutation being run. |
| struct rational pWeakUsually | .numerator = 11, .denominator = 20 | Positive bias for some mutations to have a higher score after running. |
| int minTexty | 6 | Used to determine if a line contains enough ascii text for an ascii mutation. |
| int maxMutations | 16 | The max number of mutations to run for the many and burst patterns. Used to prevent overflows if the remutate probability does not first stop a pattern. |
| struct rational remutateProbability | .numerator = 4, .denominator = 5 | The probability that the many and burst patterns will mutate again. |
| int averageBlockSize | 2048 | The average size of a chunk of bytes. Used to divide output of some mutations into reasonable chunks. |
| int minBlockSize | 256 | The minimum size of a chunk of bytes. Used to limit the smallest chunk that can be made by a generator. |
| int maxBlockSize | 4096 | The maximum size of a chunk of bytes. Used to limit the largest size chunk of bytes made by a generator. |
| int initialIp | 24 | The inverse probability, 1/24, of a chunk of bytes being chosen to be mutated |
| int maxPumpLimit | 16384 | If an xml has nodes x, y, and z in the order xyz, then the xml should be able to handle xyyz and xyyyz. This is the limit on the number of y's an xml mutation will try to insert. |

### Running the Fuzzer
After the constants are initialized and a seed is determined, the running the radamsa-rose fuzzer is very straightforward.
```C
// Create the fuzzer using defined constants and a seed
radamsa_rose fuzzer = newRadamsaRoseFuzzer(seed, constants);

...
// Run the radamsa algorithm with input in of length in_len.
// Write the output to the out buffer with a max size of max_out.
// The function returns the actual length written to the output buffer.
size_t len = runRadamsa(fuzzer, in, in_len, out, max_out);
...

// Delete the fuzzer to free up the space taken
deleteRadamsaRoseFuzzer(fuzzer);
```
The radamsa_rose fuzzer is a pointer to a radamsa rose fuzzer instance. Multiple runs can be used with this instance. Changes to the mutations scores on runs as well as the state of random will be maintained across the lifetime of a fuzzer instance. This means running the `runRadamsa(fuzzer, ...)` in a for loop of x iterations has the a similar effect as the `-n x` flag in the original radamsa. Checksumming for duplicate mutated outputs is not built into the `runRadamsa` library function. At the moment, this will have to be handled manually. Updates may add a seperate header for checksum functionality.

### Customizing the Fuzzer
The radamsa-rose fuzzing library also allows for a series of customizations to its algorithm in addition to the constants.

The fuzzing alogorithm works in this basic manner. It takes an input of specified size and will use a generator to generate byte chunks for mutation. The generation of these blocks use the _min/average/maxBlocksize_ constants. The default generators are those for **stdin** and **files** and a generator that will generate **random** data. Upon generation, a pattern will be run on the byte chunks. Patterns not only select the chunk to mutate, but also decide the number of times it will be mutated. The patterns consist of **mutate once (od), mutate many times (nd), burst mutation (bu)**. The **Mutate Once** pattern will pick a block to mutate using the _initialIp_ probability, favoring blocks towards the front of the input. The **Mutate Many Times** will pick a block in the same manner, but will also try to mutate more blocks in the input. **Burst Mutations** is similar to the many mutations, but will perform all of its mutations on one block. There are a variety of mutations that can be applied, which are listed below.

The library user has the option of disabling, enabling, and changing the priority of all present generators, patterns, and mutations. This can be done with the following functions (delimeted with commonPrefix{callOne/callTwo}):
- `enable{Pattern/Mutation/Generator}(fuzzer, char* name)`
- `disable{Pattern/Mutation/Generator}(fuzzer, char* name)`
- `enableAll{Patterns/Mutations/Generators}(fuzzer)`
- `disableAll{Patterns/Mutations/Generators}(fuzzer)`
- `set{Pattern/Mutation/Generator}Priority(fuzzer)`

For example, say you want to ignore the **Mutate Once** pattern, give a higher priority to the **Burst** pattern, and disable all but the **byte increment mutation**, you would write the following code:
```C
radamsa_rose fuzzer = newRadamsaRoseFuzzer(seed, constants);

disablePattern(fuzzer, "od"); // Disable mutate once pattern (names are the same as the original radamsa)
setPatternPriority(fuzzer, "bu", 10); // Disable mutate once pattern (names are the same as the original radamsa)
disableAllMutations(fuzzer); // Disable all mutations for this fuzzer instance
enableMutation(fuzzer, "bei"); // Enable the byte increment mutation

size_t len = runRadamsa(fuzzer, in, in_size, out, max_out_size);
...
```
This is equivalent to running the original radamsa with the `-p nd=2,bu=10 -m bei` flags. WARNING: Order matters and so enabling `bd` before `bi` will have a different effect than if the order were switched (i.e. `-m bd,bi` and `-m bi,bd` are not the same and will generate different output).

### Generators
| Generator | Priority | Description |
|-----------|----------|-------------|
| stdin | 100,000 | Will take what ever is in stdin and split it into byte chunks for mutation. In the case of the library, this is simply the buffer passed in to the runRadamsa call. |
| file | 1,000 | Will attempt to open a file pointed to by a path and read chunks of data for mutation. Paths in the input are comma seperated and are randomly chosen. If the file cannot be found, our library will simple attempt to mutate the input as is. |
| random | 1 | Ignores the input passed into the library and simply creates a random stream of bytes. Chaos ensues. |
| jump | 200 | Will take two randomly chose files in pointed to by comma separated input and attempt to generate on stream of data using various byte chunks from both files. |

The priority is the chance that the generator will get chosen. There is a very low chance to throw away input and generate a random stream.

### Patterns
The names and priority were taken from the radamsa. These determine which byte chunks will be mutated and how many times it will be mutated. At least one mutation will always be run.

| Generator | Priority | Description |
|-----------|----------|-------------|
| od | 1 | Pick a block and mutate once. Favors byte chunks towards the front of the input. |
| nd | 2 | Select various blocks of bytes to mutate. Imagning the chunks in a queue, once a chunk has been passed it will not have the chance of being mutated. |
| bu | 1 | Pick a block of bytes and nail it with mutations. This guarantees at least two mutations will be run. |

### Mutations
The priority, and the names were taken from the radamsa fuzzer. These are the actual transformations performed on a chunk of bytes chosen by a pattern. Each time a pattern selects a chunk, one of these mutations has a chance of being run. The chance of a mutation being used again is increased after a successful mutation and decreased after failure. Some mutations (such as the byte mutations) chose to randomly affect their chances of being used again.

| Mutation | Priority | Description |
|----------|:--------:|-------------|
| ab | 1 | (Ascii) If the input is determined to be textual, inserts either some predefined ascii stuff that will mess with inputs, appends a bunch of "\a" (beeps) characters, or appends a null byte to the end of the chunk. |
| bd | 1 | (Byte Drop) Pick one random byte, and remove it from the chunk |
| bf | 1 | (Byte Flip) Pick one random byte and flip 0 and 1's. |
| bi | 1 | (Byte Insert) Insert a random byte in a random position in the chunk |
| br | 1 | (Byte Repeat) Pick a random byte and repeat it a random number of times |
| bp | 1 | (Byte Permute) Pick a sublist of bytes and permute their order |
| bei | 1 | (Byte Increment) Pick a random byte and increment its value by 1. A value of 255 is overflowed to 0 |
| bed | 1 | (Byte Decrement) Pick a random byte and decrement its value by 1. A value of 0 is overflowed to 255 |
| ber | 1 | (Byte Random) Pick a random byte and swap it with a random value between 0 and 255 |
| sr | 1 | (Sequence Repeat) Repeat a sequence of bytes |
| sd | 1 | (Sequence Delete) delete a sequence of bytes |
| ld | 1 | (Line delete) If the first 8 bytes in a chunk are ascii, splits the chunk into newline delimited lines, and deletes a line |
| lds | 1 | (Line Delete Sequence) Similar to Line Delete, but deletes a continuous sequence of lines |
| lr2 | 1 | (Line Duplicate) If the first 8 bytes in a chunk are ascii, splits the chunk into newline delimited lines, and duplicates a line |
| li | 1 | (Line Insert) If the first 8 bytes in a chunk are ascii, splits the chunk into newline delimited lines, randomly selects a line, duplicates it and then inserts it somewhere else. |
| lr | 1 | (Line Repeat)  If the first 8 bytes in a chunk are ascii, splits the chunk into newline delimited lines and repeats a line a random number of times. |
| ls | 1 | (Line Swap) If the first 8 bytes in a chunk are ascii, splits the chunk into newline delimited lines and swaps two randomly chosen lines. |
| lp | 1 | (Line Permute) If the first 8 bytes in a chunk are ascii, splits the chunk into newline delimited lines. Permutes the order of a sublist of lines |
| lis | 1 | (Stateful Line Insert) Similar to line insert, but has the chance of inserting a line from a previously processed chunk. |
| lrs | 1 | (Stateful Line Replace) Similar to line replace, but has the chance of replacing a line with one from a previously processed chunk. |
| td | 1 | (Tree Delete) Looks for balanced delemiters, such as "()" or "<>", and then randomly selects a pair and deletes everything inside and the enclosing delimeters. |
| tr2 | 1 | (Tree Duplicate) Similar to Tree Delete, but simply duplicates the "node" instead of deleting it. |
| ts1 | 1 | (Tree Swap One) swap one node with another one |
| ts2 | 1 | (Tree Swap Two) swap two nodes pairwise |
| tr | 1 | (Tree Stutter) repeat a path of the parse tree |
| uw | 1 | (UTF-8 Widen) Select a byte in the chunk and try to widen it to be similar to a UTF-16 encoded codepoint |
| ui | 2 | (UTF-8 Insert) Randomly select a unicode codepoint value from a defined set of abusive values, and insert it into the byte chunk. |
| num | 5 | (Number) Identify a textual representation of a number and ludicrously modify it. |
| xp | 9 | (XML) If the byte chunk describes xml, run an xml mutation on it. XML mutations will either swap an xml node with another one (XP Swap), duplicate a node (XP Duplicate), repeat the node (XP Repeat), create a new node using the tags and duplications of sub-nodes from a similar xml node (XP Insert), or pump in nodes, taking a structure with nodes x, y, and z that looks like xyz and replacing it with xy<sup>n</sup>z (XP Pump). |
| ft | 2 | (Fuse This) Merge a suffix with a prefix of bytes in the same byte chunk that align on a sequence of bytes |
| fn | 1 | (Fuse Next) Similar to Fuse This, but fuses the next chunk to this one if available. If we have a<sub>1</sub>a<sub>2</sub> as chunk 'a' and chunk 'b' as two chunks, we create a fusion of suffixes and prefixes such that a<sub>1</sub>ba<sub>2</sub> is the result. |
| fo | 2 | (Fuse Old) Similar to Fuse Next, but merges the first half of a chunk already seen before with the first half of the current chunk. Creates a secondary chunk using the back halves. |

### Additions
Our library has also provided the ability to add your own patterns, generators, and mutations. This allows for you to take adavantage of the systems already in place to create your own fuzzing tool. If you wanted to mutate UDP packets but not accidently mutate headers, you could define your own generator that puts headers into their own chunks. In combination with a custom pattern, you could then skip over those chunks with no chance of mutation. If you a mutation that would reverse a chunk of bytes or did some more specific xml mutations, our library provides an interface to do just that. In the rose-fuzzer.h file we provide three functions:
```c
void addNewMutation(radamsa_rose radamsa, mutateFunction, char* name, int priority);
void addNewPattern(radamsa_rose radamsa, patternFunction, char* name, int priority);
void addNewGenerator(radamsa_rose radamsa, generatingFunction, char* name, int priority);
```
with specific headers for the mutate, pattern, and generating functions. These functions must be defined as follows: 
```c
#include "radamsa-rose/customize.h"

int mutation(struct input_chunk*, struct radamsa_constants, radamsa_random)
struct input_chunk* pattern(struct input_chunk*, radamsa_mutation, struct radamsa_constants, radamsa_random)
struct input_chunk* generator(const char*, size_t size, struct radamsa_constants, radamsa_random)
```
with the function names of your choosing. The `struct input_chunk*` is a pointer to a custom struct that encapsulates an integer detailing the size in bytes of the chunk, a pointer to an allocated `char*` that represents the bytes, and a pointer to the next chunk (set to `NULL` or `0` if this is the last chunk). This essentially creates a nice singly-linked list of bytes for use.

For mutations and patterns, allocation and deallocation of the chunks before and after user are handled by our system. However, if you mutations need more space, you are expected to deallocate the already existing buffer and allocate your own. The size not only tells you how big the buffer is, but also tells our system how many bytes are in your new buffer.

For mutations, the passed in pointer points to the selected chunk and is often all you need. However, if you would like to perform mutations such as Fuse-Next, the the rest of the chunks are maintained and accessible. **Be careful of allocation when messing with the order of chunks**. The mutation is also given the constants if you would like to use them and a pointer to the random stream to generate random values exactly as radamsa does. The mutation must return an integer delta. This delta will be affect the probability that the mutation is run again in the future. Radamsa normally returns a value of -1, 0, or 1.

Patterns take in a pointer to some chunk, a pointer to an radamsa mutation, constants and random. The thing to note here is that pattern returns an input chunk as well. The returned value should be the first chunk you want to output at the end. Anything before this chunk will be ignored by our system. Thus, you should often return the chunk head that you are given. However, we also provide other options as long as you *deallocate responsibly*. Since patterns are about selecting chunks, in customize.h we have provided the function `struct input_chunk* chooseChunk(struct input_chunk* head, int initialIp, radamsa_random rand)` that selects a chunk just as radamsa would and returns the pointer to it.
The radamsa_mutation is a pointer to a C++ object that can not really be run from C. Thus, customize.h provides a very useful function, `void runRadamsaMutation(radamsa_mutation muta, struct input_chunk* head)`. This function uses the provided mutation and modifies the chunk in place.

Last but not least are the generators. The goal of generators is to take input and provide a queue of byte chunks. The input should not need to be modified to do this. Thus all allocation is on the responsibility of the implementer. Deallocation will be handled by our system. If you simply want to manipulate chunk sizes, it is recommended to use the constants instead.

The full API of `customize.h` is detailed at the bottom.

## Comparison to Radamsa
As mentioned, this is supposed to be a replica of the radamsa fuzzing algorithm. As of version 0.9.6, not all of the seeds provide an exact replica output of radamsa. This is because some mutations have edge-cases or bugs that have not yet been resolved. Thus, if they are run in the middle of a run with the algorithm it usually affects the entire system, causing everything to be off. Patterns and Generators are exact as far as we have been able to determine. This is the list of known erroneous mutations:
- Tree Stutter
- UTF-8 Insert (on some seeds this mutation will work without issue)

When we say that these mutations are erroneous, that is not to say that they do not do something interesting. They are fully functional and provide interesting output. However, they do not perform the exact same as their clones in radamsa, and thus their inclusion will cause the output to be different than that of radamsa if given the same seed.

### Running N times
One important thing to note is that both Radamsa and our library can be run N times in a loop. However, Radamsa tries to generate unique output for each run by hashing its output. Thus, if N=5, Radamsa has the potential to run 6 or more times if outputs from a two separate runs are the same.

Our library has taken a different approach. We also allow for being run in a loop. Calling the runRadamsa in a loop will perform similar to the original radamsa. However, we do not provide the same functionality for guaranteeing unique output. Thus, the number of actual runs is left up to the user. 

## API
This section will provide the complete overview of the API

**radamsa-rose/rose-fuzzer.h**
```c
typedef void* radamsa_rose; 
radamsa_rose newRadamsaRoseFuzzer(int randomSeed, const struct radamsa_constants constants);
void deleteRadamsaRoseFuzzer(radamsa_rose radamsa);

size_t runRadamsa(radamsa_rose radamsa, char* input, size_t inputSize, char* target, size_t max_size);
void initDefaultConstants(struct radamsa_constants* constants);

void enableMutation(radamsa_rose radamsa, char* name);
void disableMutation(radamsa_rose radamsa, char* name);
void enableAllMutations(radamsa_rose radamsa); 
void disableAllMutations(radamsa_rose radamsa);
void setMutationPriority(radamsa_rose radamsa, char* name, int priority);

void enablePattern(radamsa_rose radamsa, char* name);
void disablePattern(radamsa_rose radamsa, char* name);
void enableAllPatterns(radamsa_rose radamsa); 
void disableAllPatterns(radamsa_rose radamsa);
void setPatternPriority(radamsa_rose radamsa, char* name, int priority);

void enableGenerator(radamsa_rose radamsa, char* name);
void disableGenerator(radamsa_rose radamsa, char* name);
void enableAllGenerators(radamsa_rose radamsa); 
void disableAllGenerators(radamsa_rose radamsa);
void setGeneratorPriority(radamsa_rose radamsa, char* name, int priority); 
```

**radamsa-rose/constants.h** (Already included in rose-fuzzer.h)
```c
struct rational {
	uint32_t numerator;
	uint32_t denominator;
};

struct radamsa_constants {
	int maxHashCount;
	int searchFuel;
	int searchStopIp;
	int storedElements;
	int updateProbability;
	int minScore;
	int maxScore;
	struct rational pWeakUsually;
	int randDeltaIncrease;
	int randDeltaDecrease;
	int minTexty;
	int maxMutations;
	int averageBlockSize;
	int minBlockSize;
	int initialIp;
	struct rational remutateProbability;
	int maxBlockSize;
	int maxPumpLimit;
};
```

**radmsa-rose/customize.h**
```c
// Functions on random
unsigned long long nextInteger(radamsa_random rand, unsigned long long max);
int doesOccur(radamsa_random rand, struct rational probability);
unsigned int nextBlockSize(radamsa_random rand, unsigned int minSize, unsigned int maxSize);
int randomDelta(radamsa_random rand);
int randomDeltaUp(radamsa_random rand, struct rational probability);
unsigned long long nextNBit(radamsa_random rand, unsigned long long n);
unsigned long long nextLogInteger(radamsa_random rand, unsigned long long max);
unsigned long long nextIntegerRange(radamsa_random rand, unsigned long long low, unsigned long long high);
uint32_t generateNext(radamsa_random rand);

// Helpers for custom patterns
struct input_chunk* chooseChunk(struct input_chunk* head, int initialIp, radamsa_random rand);
void runRadamsaMutation(radamsa_mutation muta, struct input_chunk* head);
```
