cmake_minimum_required (VERSION 3.10 FATAL_ERROR)

project(Radamsa-Rose
    VERSION 0.9.6
    LANGUAGES C CXX)

if(NOT CMAKE_BUILD_TYPE) 
    set(CMAKE_BUILD_TYPE Debug)
endif(NOT CMAKE_BUILD_TYPE)

add_subdirectory(src)

option(BUILD_RR_TESTS "Build Radamsa-Rose tests using Catch2" OFF)
option(BUILD_RADAMSA_ROSE_APP "Build an application version of the Radamsa-Rose library" OFF)

# Only enable testing in the current project
if((CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME) OR BUILD_RR_TESTS) 
    add_library(Catch INTERFACE)
    target_include_directories(Catch INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}/extern/catch)
    add_subdirectory(tests)
endif()

if (BUILD_RADAMSA_ROSE_APP)
    add_subdirectory(app)
endif()