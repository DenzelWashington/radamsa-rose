extern "C" {
    #include "radamsa-rose/customize.h"
}
#include "random/random.h"
#include "mutation/mutation.h"
#include "chunk_queue_interface.h"

using namespace Radamsa;

unsigned long long nextInteger(radamsa_random rand, unsigned long long max) {
    return ((Random*) rand)->nextInteger(max);
}

int doesOccur(radamsa_random rand, struct rational probability)  {
    return ((Random*) rand)->doesOccur(probability);
}

unsigned int nextBlockSize(radamsa_random rand, unsigned int minSize, unsigned int maxSize)  {
    return ((Random*) rand)->nextBlockSize(minSize, maxSize);
}

int randomDelta(radamsa_random rand)  {
    return ((Random*) rand)->randomDelta();
}

int randomDeltaUp(radamsa_random rand, struct rational probability)  {
    return ((Random*) rand)->randomDeltaUp(probability);
}

unsigned long long nextNBit(radamsa_random rand, unsigned long long n)  {
    return ((Random*) rand)->nextNBit(n);
}

unsigned long long nextLogInteger(radamsa_random rand, unsigned long long max)  {
    return ((Random*) rand)->nextLogInteger(max);
}

unsigned long long nextIntegerRange(radamsa_random rand, unsigned long long low, unsigned long long high)  {
    return ((Random*) rand)->nextIntegerRange(low, high);
}

uint32_t generateNext(radamsa_random rand) {
    return ((Random*) rand)->generateNext();
}

struct input_chunk* chooseChunk(struct input_chunk* head, int initialIp, radamsa_random rand) {
    int ip = nextInteger(rand, initialIp);
    while (nextInteger(rand, ip) != 0 && head->next) {
        head = head->next;
    }
    return head;
}

void runRadamsaMutation(radamsa_mutation muta, struct input_chunk* head) {
    std::deque<std::string> input = fromLinkedStruct(head);
    ((Mutation*) muta)->mutate(input);
    auto res = toLinkedStruct(input);

    // Effectively replace the chunk passed in
    if (res) {
        head->chunkSize = res->chunkSize;
        head->chunkPointer = res->chunkPointer;
        head->next = res->next;
    } else {
        head->chunkSize = 0;
        delete head->chunkPointer;
        head->next = NULL;
    }
}