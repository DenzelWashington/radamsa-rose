#include "generator.h"

/// General generator constructer
///
/// @param priority int
/// @param name string represent name of generator

Radamsa::Generator::Generator(int priority, std::string name) {
	this->priority = priority;
	this->name = name;
}

Radamsa::Generator::~Generator() {
	// Nothing to destruct
}

/// Generator related getter and setter

std::string Radamsa::Generator::getName() {
	return this->name;
}