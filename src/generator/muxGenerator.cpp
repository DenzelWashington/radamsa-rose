#include "muxGenerator.h"
#include <stdexcept>
#include <algorithm>

/// General muxGenerator constructer
///
/// @param priority int
/// @param constants radamsa_constant
/// @param random a random object

Radamsa::muxGenerator::muxGenerator(int priority, const struct radamsa_constants constants, Random& random) : Generator(priority, "mux Generator") {
	this->random = &random;
	this->constants = constants;
}

bool compareGeneratorByPriority(Radamsa::Generator* gen1, Radamsa::Generator* gen2) {
	return gen1->priority > gen2->priority;
}

/// A Generator picks a generator from stdingenerator and general generator
/// by sorting generator list and comparing generator by pripority.
///
/// @param input string 

std::deque<std::string> Radamsa::muxGenerator::generate(std::string input) {
	if (this->generatorList.empty()) {
		std::deque<std::string> oneChunk;
		oneChunk.push_back(input);
		return oneChunk;
	}
	else if (this->generatorList.size() > 1) {
		std::sort(this->generatorList.begin(), this->generatorList.end(), compareGeneratorByPriority);

		int priSum = 0;
		for (Radamsa::Generator* g : this->generatorList) {
			priSum += g->priority;
		}

		int n = this->random->nextInteger(priSum);

		for (Radamsa::Generator* g : this->generatorList) {
			int currentPri = g->priority;
			if (n < currentPri) return g->generate(input);
			n -= currentPri;
		}
	}

	return this->generatorList.front()->generate(input);
}


void Radamsa::muxGenerator::addGenerator(Radamsa::Generator* gen) {
	this->generatorList.push_back(gen);
}