#pragma once

#include "generator.h"

namespace Radamsa {
	class muxGenerator : public Generator {
	private:
		struct radamsa_constants constants;
		Random* random;
		std::vector<Radamsa::Generator*> generatorList;
	public:
		muxGenerator(int priority, struct radamsa_constants constants, Random& random);
		std::deque<std::string> generate(std::string) override;
		void addGenerator(Radamsa::Generator* gen);
	};

}