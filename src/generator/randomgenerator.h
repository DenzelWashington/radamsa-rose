#pragma once

#include "generator.h"

namespace Radamsa {
	class RandomGenerator : public Generator {
	private:
		struct radamsa_constants constants;
		Random* random;

        Radamsa::Random createRandomFromRandom();
        void checkForErrorInConstants();
        std::string nextRandomBlock(Random& rand);
	public:
		RandomGenerator(int priority, struct radamsa_constants constants, Random& random);
		std::deque<std::string> generate(std::string) override;
	};

}