#pragma once

#include "generator.h"

namespace Radamsa {
	class StdinGenerator : public Generator {
	private:
		struct radamsa_constants constants;
		Random* random;

        Radamsa::Random createRandomFromRandom();
        void checkForErrorInConstants();
        std::deque<std::string> memoizedOutput;
	public:
		StdinGenerator(int priority, struct radamsa_constants constants, Random& random);
		std::deque<std::string> generate(std::string) override;
	};
}
