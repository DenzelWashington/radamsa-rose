#pragma once
#include "mutation.h"

namespace Radamsa {
	class NumMutation : public Radamsa::Mutation {
	private:
		int delta;

		Random* random;
		struct radamsa_constants constants;
		
	public:
		// public for testing
		int mutateANum(std::string &input, int nfound);
		int getNum(std::string &input);
		int digitVal(char input);
		std::string mutateNum(int num);
		std::string copyRange(std::string pos, std::string end, std::string tail);
		// some of the numbers are way bigger than max ull, so instead of using our Bignums I made a hacky solution
		// But none of the operations on interesting numbers /should/ be bignums so I'm kinda fudging it
		// The only potential problem is if we carry out a number, but it's functionally close enough to 
		// Radamsa that I'm not going to sweat it.
		// Max ULL = 18,446,744,073,709,551,615
		const std::string interestingNumsTopDigits[33] = {"340282366920938463463","340282366920938463463","340282366920938463463", "170141183460469231731","170141183460469231731","170141183460469231731", "1","1","1", "","","", "","","", "","","", "","","", "","","", "","","", "","","", "","",""};
		const unsigned long long int interestingNums[33] = {374607431768211455, 374607431768211456, 374607431768211457,  // 2^128
															687303715884105727, 687303715884105728, 687303715884105729,  // 2^127
															8446744073709551615, 8446744073709551616, 8446744073709551617, // max ULL 2^64 - these are where top digits come into play
															9223372036854775807,  9223372036854775808ULL,  9223372036854775809ULL, //Max LL
															4294967295, 4294967296, 4294967297, // Max unsigned int
															2147483647, 2147483648, 2147483649, // Max int
															65535, 65536, 65537, // 2 Bytes
															32767, 32768, 32769, // Byte size
															255, 256, 257, 
															127, 128, 129,
															1, 2, 3 };
		NumMutation(int pri, struct radamsa_constants constants, Random& random);
		std::string mutate(std::string input);
		int nextDelta();
	};
}