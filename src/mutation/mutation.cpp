#include "mutation.h"
#include <algorithm>

/// General Mutation Constructer
///
/// @param score an int
/// @param pri an int
/// @param name string represent name of the mutation

Radamsa::Mutation::Mutation(int score, int pri, std::string name) {
	this->score = score;
	this->priority = pri;
	this->name = name;
}

Radamsa::Mutation::~Mutation() {
	// Nothing to destruct
}

void Radamsa::Mutation::mutate(std::deque<std::string>& input) {
	std::string firstBlock = input.front();
	input.pop_front();
	firstBlock = this->mutate(firstBlock);
	input.push_front(firstBlock);
}

std::string Radamsa::Mutation::getName() {
	return this->name;
}