#pragma once
#include "mutation.h"
#include <queue>
#include <tuple>
#include <list>

namespace Radamsa {
	bool compareMutaPriorities(std::pair<int, Radamsa::Mutation*> p1, std::pair<int, Radamsa::Mutation*> p2);

	class muxFuzzer : public Radamsa::Mutation {
	private:
		struct radamsa_constants constants;
		Random* random;
		std::list<Radamsa::Mutation*> mutationList;
		std::priority_queue<std::tuple<int, int, Radamsa::Mutation*> > mutationQueue;

		void weightedPermutation();
		int adjustPriority(int pri, int delta);
		void emptyPriorityQueue();
	public:
		muxFuzzer(int pri, struct radamsa_constants constants, Random& random);
		void mutate(std::deque<std::string>& input) override;
		std::string mutate(std::string input) override;
		int nextDelta() override;
		void addMutation(Radamsa::Mutation* mut);
	};
}