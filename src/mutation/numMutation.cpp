#include "numMutation.h"

Radamsa::NumMutation::NumMutation(int pri, struct radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "num") {
	this->delta = 0;
	this->random = &random;
	this->constants = constants;
}

std::string Radamsa::NumMutation::copyRange(std::string pos, std::string end, std::string tail) {
	std::string out = "";
	
	while (pos != end) {
		out = out + pos.substr(0, 1);
		pos = pos.substr(1, pos.length() - 1);
	}

	return out + tail;
}

std::string Radamsa::NumMutation::mutateNum(int num) {
	int index = 0;
	long long int val = 0;
	unsigned long long int newNum = 0;
	switch (random->nextInteger(12)) {
	case 0:
		return std::to_string(num + 1);
		break;
	case 1:
		return std::to_string(num - 1);
		break;
	case 2:
		return std::to_string(0);
		break;
	case 3:
		return std::to_string(1);
		break;
	case 4:
	case 5:
	case 6:
		index = random->nextInteger(33);
		return interestingNumsTopDigits[index] + std::to_string(interestingNums[index]);
		break;
	case 7:
		index = random->nextInteger(33);
		newNum = interestingNums[index] + num;
		return interestingNumsTopDigits[index] + std::to_string(newNum);
		break;
	case 8:
		index = random->nextInteger(33);
		val = interestingNums[index] - num; // since we have subtraction it can be negative
		return interestingNumsTopDigits[index] + std::to_string(val);
		break;
	case 9:
		return std::to_string(num - random->nextInteger(num * 2));
		break;
	case 10:
	case 11:
		unsigned long long int n = random->nextIntegerRange(1, 129);
		Bignum log = random->nextLogInteger(n);
		int s = random->nextInteger(3); 
		if (s == 0) { // addition more likely
			// This will truncate bignums because they cast down to ULL
			// Fix would be to make num a bignum and allow for negatives
			Bignum out = std::abs((long) num - (long) log);
			return "-" + out.toString();
		}
		else {
			const unsigned int a = num;
			Bignum out = log + a;
			return out.toString();
		}
		break;
	}
	// Failure - we should never reach here.
	return "Bad Number Mutation Happened";
}

int Radamsa::NumMutation::digitVal(char input) {
	int out = input;

	if (out >= 48 && out <= 57) { // ascii 0-9
		return out - 48;
	}
	
	return -1;
}

int Radamsa::NumMutation::getNum(std::string &input) {
	int digits = 0;
	int n = 0;

	while (true) {
		if (input.length() == 0) {
			if (digits == 0) return -1;
			return n;
		}
		else if (digitVal(input[0]) != -1) {
			n = digitVal(input[0]) + (n * 10);
			digits++;
			input = input.substr(1, input.length() - 1);
		}
		else if (digits == 0) {
			return -1;
		}
		else {
			return n;
		}
	}
}

int Radamsa::NumMutation::mutateANum(std::string &input, int nfound) {
	if (input.length() == 0) {
		return random->nextInteger(nfound); // Choose which to mutate, or 0 if none
	};

	std::string listp(input);

	int val = getNum(listp); // listp gets chopped down to everything after the number
	if (val != -1) {
		std::string tail(listp);
		int which = mutateANum(tail, nfound + 1); // listp functions as our tail here.
		if (which == 0) { // I won
			std::string newNum = mutateNum(val);
			input = newNum + tail;
			return -1; // we're done.
		}
		else {
			input = copyRange(input, listp, tail);
		}
	}
	else { // didn't find a number
		std::string tail(input.substr(1, input.length() - 1));
		int which = mutateANum(tail, nfound);
		input = input.at(0) + tail; // we go down char by char, we come back that way too.
		return which;
	}
}

std::string Radamsa::NumMutation::mutate(std::string input) {
	std::string out(input);
	int n = mutateANum(out, 0);
	//bool bin = isBinarish(input);
	if (n == 0) { // Did not find a number
		random->nextInteger(10) == 0 ? delta = -1 : delta = 0;
	}
	else if (false) { //bin)
		// found, but this looks a bit binary
		delta = -1;
	}
	else {
		delta = 2;
	}
	return out;
}

int Radamsa::NumMutation::nextDelta() {
	return delta;
}