#include "abstract_fuse_mutation.h"
#include "fuse.h"

Radamsa::AbstractFuseMutation::AbstractFuseMutation(int priotity, const struct radamsa_constants constants, Random& random, std::string name) : Mutation(constants.maxScore, priotity, name) {
    this->random = &random;
    this->constants = constants;
}

int Radamsa::AbstractFuseMutation::nextDelta() {
    if (this->random->doesOccur(this->constants.pWeakUsually)) {
        return 1;
    }
    return -1;
}

std::vector<char> Radamsa::AbstractFuseMutation::fuse(std::vector<char> listOne, std::vector<char> listTwo) {
    return Radamsa::fuse(listOne, listTwo, *(this->random), this->constants);
}