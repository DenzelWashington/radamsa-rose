#pragma once
#include "abstract_fuse_mutation.h"

namespace Radamsa {
    class FuseOldMutation : public AbstractFuseMutation {
    public:
        FuseOldMutation(int priotity, const struct radamsa_constants constants, Random& random);
        void mutate(std::deque<std::string>& input) override;
        std::string mutate(std::string input) override;
    private:
        std::string rememberedBlock;
        bool remembering;
    };
}