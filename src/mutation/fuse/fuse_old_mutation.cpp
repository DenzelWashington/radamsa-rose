#include "fuse_old_mutation.h"

#define toVec(s) std::vector<char>(s.begin(), s.end())

Radamsa::FuseOldMutation::FuseOldMutation(int priotity, const struct radamsa_constants constants, Random& random) : AbstractFuseMutation(priotity, constants, random, "fo") {
    this->remembering = false;
}

void Radamsa::FuseOldMutation::mutate(std::deque<std::string>& input) {
    if (!this->remembering) {
        this->rememberedBlock = input.front();
        this->remembering = true;
    }

    std::string firstBlock = input.front();
    input.pop_front();
    std::string fbHalfA = firstBlock.substr(0, (firstBlock.size() + 1) / 2);
    std::string fbHalfB = firstBlock.substr((firstBlock.size() + 1) / 2);

    std::string rbHalfA = this->rememberedBlock.substr(0, (this->rememberedBlock.size() + 1) / 2);
    std::string rbHalfB = this->rememberedBlock.substr((this->rememberedBlock.size() + 1) / 2);
    
    std::vector<char> newFirstBlock = this->fuse(toVec(fbHalfA), toVec(rbHalfA));
    std::vector<char> newSecondBlock = this->fuse(toVec(rbHalfB), toVec(fbHalfB));

    input.push_front(std::string(newSecondBlock.begin(), newSecondBlock.end()));
    input.push_front(std::string(newFirstBlock.begin(), newFirstBlock.end()));

    if (this->random->nextInteger(3) == 0) {
        this->rememberedBlock = firstBlock;
    }
}

std::string Radamsa::FuseOldMutation::mutate(std::string input) {
    // This function is not needed by fuse-old
    return input;
}