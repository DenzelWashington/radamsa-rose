#pragma once
#include "mutation.h"

namespace Radamsa {
	class utf8InsertMutation : public Radamsa::Mutation {
	private:
		int del;
		struct radamsa_constants constants;
		Random* random;
	public: 
		utf8InsertMutation(int pri, struct radamsa_constants constants, Random& random);
		std::string mutate(std::string input) override;
		int nextDelta() override;
	};
}