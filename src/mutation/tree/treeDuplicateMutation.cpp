#include "treeDuplicateMutation.h"
#include "treeUtils.h"

Radamsa::TreeDuplicateMutation::TreeDuplicateMutation(int pri, struct radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "tr2")
{
	this->delta = 1; // this is dumb, he should have a -1 when the tree doesn't parse out, but it's what Radamsa does.
	this->random = &random;
	this->constants = constants;
};

std::string Radamsa::TreeDuplicateMutation::mutate(std::string input) {
	TreeUtils utils = TreeUtils();

	std::map<std::string, std::string> sublists = utils.sublists(input);

	if (!sublists.empty()) {
		auto it = sublists.begin();
		int n = random->nextInteger(sublists.size());

		for (; n > 0; n--, it++);
		it->second = it->second + it->second;
	}

	utils.flatten(input, sublists);
	return input;
}

int Radamsa::TreeDuplicateMutation::nextDelta() {
	return delta;
}