#pragma once
#include "../mutation.h"

namespace Radamsa {
	class TreeDuplicateMutation : public Radamsa::Mutation {
	private:
		Random* random;
		struct radamsa_constants constants;
		int delta;
	public:
		TreeDuplicateMutation(int pri, struct radamsa_constants constants, Random& random);
		std::string mutate(std::string input);
		int nextDelta();
	};
}