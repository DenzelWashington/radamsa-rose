#include "treeDeleteMutation.h"
#include "treeUtils.h"

Radamsa::TreeDeleteMutation::TreeDeleteMutation(int pri, struct radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "td")
{
	this->delta = 1; // this is dumb, he should have a -1 when the tree doesn't parse out, but it's what Radamsa does.
	this->random = &random;
	this->constants = constants;
};

std::string Radamsa::TreeDeleteMutation::mutate(std::string input) {
	TreeUtils utils = TreeUtils();

	std::map<std::string, std::string> sublists = utils.sublists(input);

	if (!sublists.empty()) {
		auto it = sublists.begin();
		for (int n = random->nextInteger(sublists.size()); n > 0; n--, it++);
		it->second = "";
	}

	utils.flatten(input, sublists);
	return input;
}

int Radamsa::TreeDeleteMutation::nextDelta() {
	return delta;
}