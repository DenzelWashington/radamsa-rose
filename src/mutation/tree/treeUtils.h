#pragma once
#include "random/random.h"
#include <string>
#include <map>

namespace Radamsa {
	class TreeUtils {
	public:
		std::map<char, char> usual_delims;
		TreeUtils();
		//static std::vector<std::string>> partialParse(std::string input);
		// Each <string, string> pair is a "node" in aki's tree
		// They are coded <"#1#", "substring">, and the original string is edited to be ~~~~~#1#~~~~~ where substring was.
		// We then go back and fix that before outputting.
		// The int is the index of the first character in the string, so we know where to mess with stuff in mutations
		std::map<std::string, std::string> sublists(std::string &input);
		bool isBinarish(std::string line);
		void flatten(std::string& input, std::map<std::string, std::string> sublists);
	};
}