#include "utf8InsertMutation.h"

#include <locale>
#include <codecvt>

std::wstring_convert<std::codecvt_utf8<char32_t>,char32_t> conversion;

void addRange(char32_t start, char32_t end, std::vector<std::u32string>& vec) {
    for (char32_t i = start; i <= end; ++i) {
        std::u32string s( { i } );
        vec.push_back(s);
    }
}

std::vector<std::u32string> initCodepoints() {
    std::vector<std::u32string> funny;

    // The valid codepoints and ranges in mutations.scm
    funny.push_back(U"\U0001F4A9");
    addRange(0x0E40, 0x0E44, funny);
    addRange(0xE0020, 0xE007F, funny);
    funny.push_back(U"\U000E0001");
    funny.push_back(U"\uffa0");
    funny.push_back(U"\u3164");
    addRange(0x2065, 0x2069, funny);
    addRange(0x115F, 0x1160, funny);
    funny.push_back(U"\u034F");
    funny.push_back(U"\uFDD0");
    addRange(0x02B0, 0x02B8, funny);
    funny.push_back(U"\uFF70");
    addRange(0x02D0, 0x02D1, funny);
    funny.push_back(U"\u00B7");
    funny.push_back(U"\u0345");
    addRange(0x200C, 0x200D, funny);
    addRange(0x202A, 0x202E, funny);
    addRange(0x200E, 0x200F, funny);
    funny.push_back(U"\u3000");
    funny.push_back(U"\u205f");
    funny.push_back(U"\u202f");
    funny.push_back(U"\u2029");
    funny.push_back(U"\u2028");
    addRange(0x2000, 0x200A, funny);
    funny.push_back(U"\u180e");
    funny.push_back(U"\u1680");
    funny.push_back(U"\u00a0");
    addRange(0x0009, 0x000d, funny);

    return funny;
}

// UTF-32 encoded unicode codepoints (good luck)
std::vector<std::string> initFunny() {
    std::vector<std::string> funny;

    //Aki's manual codepoints
    funny.push_back("\xe2\x80\xae");
    funny.push_back("\xe2\x80\xad");
    funny.push_back("\xe1\xa0\x8e");
    funny.push_back("\xe2\x81\xa0");
    funny.push_back("\xef\xbb\xbe");
    funny.push_back("\xef\xbf\xbf");
    funny.push_back("\xe0\xbf\xad");
    funny.push_back("\xed\xba\xad");
    funny.push_back("\xed\xaa\xad");
    funny.push_back("\xef\xa3\xbf");
    funny.push_back("\xef\xbc\x8f");
    funny.push_back("\xf0\x9d\x9f\x96");
    funny.push_back("\xc3\x9f");
    funny.push_back("\xef\xb7\xba");
    funny.push_back("\xce\x90");
    funny.push_back("\xe1\xbe\x82");
    funny.push_back("\xef\xac\xac");
    funny.push_back("\xf0\x9d\x85\xa0");
    funny.push_back("\xf4\x8f\xbf\xbe");
    funny.push_back(std::string{ (char)239, (char)191, (char)191 });
    funny.push_back(std::string{ (char)240, (char)144, (char)128, (char)128});
    funny.push_back("\xef\xbb\xbf");
    funny.push_back("\xFE\xFF"); // UTF16 Big Endian BOM
    funny.push_back("\xFF\xFE"); // UTF16 Little Endian BOM
    funny.push_back(std::string("\x0\x0\xff\xff", 4)); // ascii null Big Endian
    funny.push_back(std::string("\xff\xff\x0\x0", 4)); // ascii null Little Endian
    funny.push_back(std::string{43, 47, 118, 56});
    funny.push_back(std::string{43, 47, 118, 57});
    funny.push_back(std::string{43, 47, 118, 43});
    funny.push_back(std::string{43, 47, 118, 47});
    funny.push_back(std::string{(char)247, 100, 76});
    funny.push_back(std::string{(char)221, 115, 102, 115});
    funny.push_back(std::string{14, (char)254, (char)255});
    funny.push_back(std::string{(char)251, (char)238, 40});
    funny.push_back(std::string{(char)251, (char)238, 40, (char)255});
    funny.push_back(std::string{(char)132, 49, (char)149, 51});

    // Add valid code points
    for (auto unicode : initCodepoints()) {
        funny.push_back(conversion.to_bytes(unicode));
    }

    return funny;
}


// Funny string in mutations.scm
const std::vector<std::string> funnyUnicode(initFunny());


/// utf8InsertMutation Constructer
///
/// @param pri an int argument
/// @param constants a radamsa_constan argument
/// @param random a Random pointer
Radamsa::utf8InsertMutation::utf8InsertMutation(int pri, radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "ui") {
	this->constants = constants;
	this->random = &random;
    this->del = 0;
}

/// Mutation that insert a unicode in funnycode list at random index
///
/// @param pri an int argument
/// @param constants a radamsa_constan argument
/// @param random a Random pointer

std::string Radamsa::utf8InsertMutation::mutate(std::string input) {
    int pos = random->nextInteger(input.length());
    del = random->randomDelta();
    int whichFunny = random->nextInteger(funnyUnicode.size());
    if (!input.empty()) {
        std::string unicodeBytes = funnyUnicode[whichFunny];
        input.insert(pos, unicodeBytes);
    }
    return input;
}

    
int Radamsa::utf8InsertMutation::nextDelta() {
	return del;
}