#pragma once
#include "mutation.h"
#include "radamsa-rose/radamsa_definitions.h"

namespace Radamsa {
	class MutationAdapter : public Mutation {
	private:
		int (*mutateFunction)(struct input_chunk*, struct radamsa_constants, radamsa_random);
		struct radamsa_constants constants;
        int delta;
        Random* random;
	public:
		MutationAdapter(int (*mutateFunction)(struct input_chunk*, struct radamsa_constants, radamsa_random), std::string name, int priority, radamsa_constants constants, Radamsa::Random& random);
		void mutate(std::deque<std::string>& input) override;
		std::string mutate(std::string input) override;
		int nextDelta() override;
	};
}