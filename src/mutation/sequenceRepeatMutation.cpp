#include "sequenceRepeatMutation.h"

Radamsa::SequenceRepeatMutation::SequenceRepeatMutation(int pri, struct radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "sr")
{
	this->delta = 0;
	this->random = &random;
	this->constants = constants;
};

void Radamsa::SequenceRepeatMutation::mutate(std::deque<std::string>& input) {
	if (input.front().size() < 2) {
		this->delta = 0;
		return;
	}

	std::string front = input.front();
	input.pop_front();
	int len = front.size();

	int startIndex = random->nextIntegerRange(0, len - 1ULL);
	int endIndex = random->nextIntegerRange(startIndex + 1ULL, len);

	std::string pre = front.substr(0, startIndex);
	std::string post = front.substr(endIndex);

	std::string stutter = front.substr(startIndex, endIndex - startIndex);

	int n = random->nextLogInteger(10);
	n = std::max(2, n);

	if (!post.empty()){
		input.push_front(post);
	}
	for (int i = 0; i < n; i++) input.push_front(stutter);
	this->delta = random->randomDelta();

	if (!pre.empty()) {
		input.push_front(pre);
	}
}

std::string Radamsa::SequenceRepeatMutation::mutate(std::string input)
{
	return input; // Should never be called
}

int Radamsa::SequenceRepeatMutation::nextDelta() {
	return this->delta;
}