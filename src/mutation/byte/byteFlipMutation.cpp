#include "byteFlipMutation.h"



/// Byte flip mutation construstor 
///
/// @param pri an int argument
/// @param constants a radamsa_constan argument
/// @param random a Random pointer

Radamsa::ByteFlipMutation::ByteFlipMutation(int pri, radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "bf") {
	this->constants = constants;
	this->random = &random;
}

/// A mutation flip byte
///
/// This mutation flips byte values at random position by xor.
/// If input string is empty, it will insert mask value.
///
/// @param input an string


std::string Radamsa::ByteFlipMutation::mutate(std::string input) {
	int len = input.length();
	int index = this->random->nextInteger(len);
	int pos = this->random->nextInteger(8);
	this->delta = this->random->randomDelta();
	if (len > 0) {
		int mask = 1 << pos;
		input[index] ^= mask;
		return input;
	}
	return input;
}

int Radamsa::ByteFlipMutation::nextDelta() {
	return this->delta;
}

