#include "byteRepeatMutation.h"

/// Byte repeat mutation construstor 
///
/// @param pri an int argument
/// @param constants a radamsa_constan argument
/// @param random a Random pointer

Radamsa::ByteRepeatMutation::ByteRepeatMutation(int pri, radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "br") {
	this->constants = constants;
	this->random = &random;
}

/// A mutation repeat byte
///
/// This mutation repeats a random byte.
/// If the input is empty string, 
/// the mutation returns empty string.
///
/// @param input an string

std::string Radamsa::ByteRepeatMutation::mutate(std::string input) {
	int len = input.length();
	int index = this->random->nextInteger(len);
	this->delta = this->random->randomDelta();
	int repetitions = repeatLen();
	if (len > 0) {
		input.insert(index, repetitions, input[index]);
		return input;
	}
	return input;
}

int Radamsa::ByteRepeatMutation::nextDelta() {
	return this->delta;
}

int Radamsa::ByteRepeatMutation::repeatLen() {	
	int limit = 2; // 0b10
	while (1) {
		int x = this->random->nextInteger(2);
		if (x == 0) return this->random->nextInteger(limit);
		if (limit == 131072) return this->random->nextInteger(limit); // 131072 = #x20000
		limit = limit << 1;
	}
}


