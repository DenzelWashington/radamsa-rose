#include "byteDecreaseMutation.h"

/// Byte decrease mutation construstor 
///
/// @param pri an int argument
/// @param constants a radamsa_constan argument
/// @param random a Random pointer

Radamsa::ByteDecreaseMutation::ByteDecreaseMutation(int pri, radamsa_constants constants, Random& random) : Mutation(constants.maxScore, pri, "bed") {
	this->constants = constants;
	this->random = &random;
}

/// A mutation decrease byte
///
/// This mutation decreases a random not-empty byte value by one.
/// If input string is empty or random index is empty,
/// the mutation will insert char(255) which is non-breaking space.
///
/// @param input an string


std::string Radamsa::ByteDecreaseMutation::mutate(std::string input) {
	int len = input.length();
	int index = this->random->nextInteger(len);
	this->delta = this->random->randomDelta();
	if (len > 0) {
		if (input[index] == 0) {
			input[index] = char(255);
		}
		else {
			input[index] = char(input[index] - 1);
		}
		return input;
	}
	return input;
}

int Radamsa::ByteDecreaseMutation::nextDelta() {
	return this->delta;
}

