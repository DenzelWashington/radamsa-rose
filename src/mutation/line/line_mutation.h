#pragma once
#include "mutation/mutation.h"
#include "random/random.h"
#include "radamsa-rose/constants.h"
#include "line_state.h"
#include <functional>

namespace Radamsa {
	class AbstractLineMutation : public Mutation {
	public:
		AbstractLineMutation(int priotity, const struct radamsa_constants constants, Random& random, std::string name);
		std::string mutate(std::string input) override;
		int nextDelta() override; // 1 if we can mutate lines, -1 otherwise
	protected:
		Random* random;
		struct radamsa_constants constants;
		virtual void performOperation(std::vector<std::string>& lines) = 0;
	private:
		bool canMutate;
		std::vector<std::string> splitLines(std::string input);
		bool isBinarish(std::string line);
		std::string mergeLines(std::vector<std::string> lines);
		bool isNullByte(const char c);
		bool hasHighBit(const char c);
	};

	class StatelessLineMutation : public AbstractLineMutation {
	public:
		StatelessLineMutation(int priotity, const struct radamsa_constants constants, Random& random, std::function<void(std::vector<std::string>&, Random*)> operation, std::string name);
	protected:
		void performOperation(std::vector<std::string>& lines) override;
	private:
		std::function<void(std::vector<std::string>&, Random*)> operation;
	};

	class StatefulLineMutation : public AbstractLineMutation {
	public:
		StatefulLineMutation(int priotity, const struct radamsa_constants constants, Random& random, std::function<void(std::vector<std::string>&, LineState&, Random*)> operation, std::string name);
	protected:
		void performOperation(std::vector<std::string>& lines) override;
	private:
		LineState state;
		std::function<void(std::vector<std::string>&, LineState&, Random*)> operation;
	};
}