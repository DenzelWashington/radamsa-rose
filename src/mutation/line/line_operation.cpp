#include "mutation/line/line_operation.h"
#include <algorithm>

/// delete lines at random index
///
/// @param lines point of vector of string
/// @param rand random object


void Radamsa::deleteLineOperation(std::vector<std::string>& lines, Random* rand) {
	if (!lines.empty()) {
		int index = rand->nextInteger(lines.size());
		lines.erase(lines.begin() + index);
	}
}

/// delete lines sequence at random index
///
/// @param lines point of vector of string
/// @param rand random object


void Radamsa::deleteLineSequenceOperation(std::vector<std::string>& lines, Random* rand) {
	if (!lines.empty()) {
		int startIndex = rand->nextInteger(lines.size());
		int length = rand->nextInteger(lines.size() - startIndex);
		auto startIterator = lines.begin() + startIndex;
		lines.erase(startIterator, startIterator + length);
	}
}

/// duplicate part of lines at random index
///
/// @param lines point of vector of string
/// @param rand random object


void Radamsa::duplicateLineOperation(std::vector<std::string>& lines, Random* rand) {
	if (!lines.empty()) {
		int index = rand->nextInteger(lines.size());
		lines.insert(lines.begin() + index, lines.at(index));
	}
}

/// duplicate part of lines start at random index, and end at random index
///
/// @param lines point of vector of string
/// @param rand random object


void Radamsa::cloneLineOperation(std::vector<std::string>& lines, Random* rand) {
	if (!lines.empty()) {
		int from = rand->nextInteger(lines.size());
		int to = rand->nextInteger(lines.size());
		lines.insert(lines.begin() + to, lines.at(from));
	}
}

/// repeat duplicate part of lines at random index random times
///
/// @param lines point of vector of string
/// @param rand random object

// Number of repetitions could possibly be a constant


// Number of repetitions could possibly be a constant
void Radamsa::repeatLineOperation(std::vector<std::string>& lines, Random* rand) {
	if (!lines.empty()) {
		int lineIndex = rand->nextInteger(lines.size());
		Bignum log = rand->nextLogInteger(10);
		unsigned int numberOfRepetitions = (unsigned int) std::max(2ULL, (unsigned long long) log);
		std::string line = lines.at(lineIndex);
		for (int i = 0; i < numberOfRepetitions; i++) {
			lines.insert(lines.begin() + lineIndex, line);
		}
	}
}

/// swap part of lines at random index and random index + 1
///
/// @param lines point of vector of string
/// @param rand random object


void Radamsa::swapLineOperation(std::vector<std::string>& lines, Random* rand) {
	if (lines.size() >= 2) {
		size_t index = rand->nextInteger(lines.size() - 1);
		std::string val = lines[index + 1];
		lines[index + 1] = lines[index];
		lines[index] = val;
	}
}

// Is next log 10 a constant?
void Radamsa::permuteLineOperation(std::vector<std::string>& lines, Random* rand) {
	if (lines.size() >= 3) {
		size_t start = rand->nextInteger(lines.size() - 3);
		unsigned int a = (unsigned int) rand->nextIntegerRange(2, lines.size() - start);
		unsigned int b = (unsigned int) rand->nextLogInteger(10);
		unsigned int size = std::max(2U, std::min(a, b));
		rand->permute(lines, start, start + size);
	}
}

/// insert pickLine result into lines at random index
/// pickLine picks parts of line in random size
///
/// @param lines point of vector of string
/// @param state lineState object
/// @param rand random object


void Radamsa::insertLineOperation(std::vector<std::string>& lines, LineState& state, Random* rand) {
	if (!lines.empty()) {
		state.step(lines);
		std::string line = state.pickLine();
		int index = (int) rand->nextInteger(lines.size());
		lines.insert(lines.cbegin() + index, line);
	}
}

/// replace lines at random index with the pickLine result
/// pickLine picks parts of line in random size
///
/// @param lines point of vector of string
/// @param state lineState object
/// @param rand random object


void Radamsa::replaceLineOperation(std::vector<std::string>& lines, LineState& state, Random* rand) {
	if (!lines.empty()) {
		state.step(lines);
		std::string line = state.pickLine();
		int index = rand->nextInteger(lines.size());
		lines[index] = line;
	}
}