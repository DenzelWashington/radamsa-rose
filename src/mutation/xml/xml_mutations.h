#pragma once
#include "random/random.h"
#include "radamsa-rose/constants.h"
#include "xpUtils.h"
#include <vector>
#include <functional>

namespace Radamsa {
    void xml_mutate(Random& rand, std::vector<Tag>& tags, std::vector<std::shared_ptr<XpNode>>& nodes, const radamsa_constants constants);
    std::function<void(Random&, std::vector<Tag>&, std::vector<std::shared_ptr<XpNode>>&, const radamsa_constants)> selectMutation(Random& rand);

    // The six mutations potentially selected by selectMutation
    void xp_swap(Random& rand, std::vector<Tag>& tags, std::vector<std::shared_ptr<XpNode>>& nodes, const radamsa_constants constants);
    void xp_dup(Random& rand, std::vector<Tag>& tags, std::vector<std::shared_ptr<XpNode>>& nodes, const radamsa_constants constants);
    void xp_pump(Random& rand, std::vector<Tag>& tags, std::vector<std::shared_ptr<XpNode>>& nodes, const radamsa_constants constants);
    void xp_repeat(Random& rand, std::vector<Tag>& tags, std::vector<std::shared_ptr<XpNode>>& nodes, const radamsa_constants constants);
    void xp_insert(Random& rand, std::vector<Tag>& tags, std::vector<std::shared_ptr<XpNode>>& nodes, const radamsa_constants constants);
}