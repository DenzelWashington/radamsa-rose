#pragma once
#include <memory>
#include <string>
#include <vector>
#include <map>

namespace Radamsa {

    enum class NodeType {Byte, Open, OpenSingle, Close, Tag, Plus};

    class XpNode {
    private:
        NodeType type;
    public:
        unsigned long long creationNumber; // public for a really janky clone hack
        XpNode(NodeType type);
        virtual ~XpNode();
        NodeType getType();
        virtual std::string toString() = 0;
        virtual std::shared_ptr<XpNode> clone() = 0;

        friend bool operator<(const XpNode& lhs, const XpNode& rhs);
        virtual bool operator==(XpNode& other) const; // Tag and Plus need to override this to compare inner nodes
    };

    bool operator<(const XpNode& lhs, const XpNode& rhs);
    bool operator>(const XpNode& lhs, const XpNode& rhs);
    bool operator<=(const XpNode& lhs, const XpNode& rhs);
    bool operator>=(const XpNode& lhs, const XpNode& rhs);
    bool operator==(const XpNode& lhs, const XpNode& rhs);
    bool operator!=(const XpNode& lhs, const XpNode& rhs);

    class ByteNode : public XpNode {
    private:
        std::string bytes;
    public:
        ByteNode(std::string bytes);
        std::string toString() override;
        std::shared_ptr<XpNode> clone() override;
    };

    class OpenNode : public XpNode {
    private:
        std::string name;
        std::map<std::string, std::string> attributes;
    public:
        OpenNode(std::string name, std::map<std::string, std::string> attributes);
        std::string toString() override;
        std::shared_ptr<XpNode> clone() override;
    };

    class OpenSingleNode : public XpNode {
        std::string name;
        std::map<std::string, std::string> attributes;
    public:
        OpenSingleNode(std::string name, std::map<std::string, std::string> attributes);
        std::string toString() override;
        std::shared_ptr<XpNode> clone() override;
    };

    class CloseNode : public XpNode {
        std::string name;
    public:
        CloseNode(std::string name);
        std::string toString() override;
        std::shared_ptr<XpNode> clone() override;
    };

    class TagNode : public XpNode {
    private:
        std::string name;
        std::map<std::string, std::string> attributes;
    public:
        std::vector<std::shared_ptr<XpNode>> xp;
        TagNode(std::string name, std::map<std::string, std::string> attributes, std::vector<std::shared_ptr<XpNode>> xp);
        std::string toString() override;
        std::shared_ptr<XpNode> clone() override;
        bool operator==(XpNode& other) const override;
    };

    class PlusNode : public XpNode {
    public:
        std::vector<std::shared_ptr<XpNode>> xp1;
        std::vector<std::shared_ptr<XpNode>> xp2;

        PlusNode(std::shared_ptr<XpNode> x1, std::shared_ptr<XpNode> x2);
        PlusNode(std::vector<std::shared_ptr<XpNode>> xp1, std::vector<std::shared_ptr<XpNode>> xp2);
        std::string toString() override;
        std::shared_ptr<XpNode> clone() override;
        bool operator==(XpNode& other) const override;
    };
}