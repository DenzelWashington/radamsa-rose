#include "xpUtils.h"
#include <regex>

std::string Radamsa::XpUtils::extractName(std::string tag) {
	int start = tag[1] == '/' ? 2 : 1;
	for (int i = start; i < tag.length(); i++) {
		if (tag[i] == ' ' || tag[i] == '>' || tag[i] == '/') {
			return tag.substr(start, i - start);
		}
	}
	return "";
}

std::map<std::string, std::string> Radamsa::XpUtils::extractAttributes(std::string tag)
{	
	std::string tagClone = std::string(tag);
	std::map<std::string, std::string> out;

	while (tagClone.find("=") != std::string::npos) {
		int index = tagClone.find("=");
		int i = index;
		for (; i >= 0; i--) {
			if (tagClone[i] == ' ') {
				i++;
				break;
			}
		}
		std::string key = tagClone.substr(i, index - i);
		i = index + 2; //skip the first " or '
		for (; i < tagClone.length(); i++) {
			if (tagClone[i] == '\'' || tagClone[i] == '\"') {
				i++;
				break;
			}
		}
		std::string val = tagClone.substr(index + 1, i - (index + 1));

		out.insert(std::pair<std::string, std::string>(key, val));

		tagClone = tagClone.substr(i);
	}

	return out;
}

std::shared_ptr<Radamsa::XpNode> Radamsa::XpUtils::constructTagNode(std::vector<std::string> matches, int openIndex, int closeIndex, std::string& modifiable)
{
	std::string name = extractName(matches[openIndex]);
	std::map<std::string, std::string> attrs = extractAttributes(matches[openIndex]);
	std::vector<std::shared_ptr<XpNode>> internalXML;

	for (auto pair : attrs) {
		modifiable = modifiable.substr(pair.first.size() + pair.second.size() + 2); // space, =, and words
	}
	modifiable = modifiable.substr(name.length() + 2);

	for (int i = openIndex + 1; i < closeIndex;) {
		std::string tag = matches.at(i);
		int index = modifiable.find(tag);
		
		// If there's anything preceding this, there's junk data in there.
		if (index > 0) {
			std::string prefix = modifiable.substr(0, index);
			std::shared_ptr<XpNode> node = std::make_shared<ByteNode>(prefix);
			internalXML.push_back(node);
			modifiable = modifiable.substr(index);
		}

		if (tag[1] == '/') { // closing tag before an opening tag. Hm.
			std::string closedName = extractName(tag);
			std::shared_ptr<XpNode> node = std::make_shared<CloseNode>(closedName);
			internalXML.push_back(node);

			modifiable = modifiable.substr(closedName.size() + 3);
			i++;
		}
		else if (tag[(tag.length() - 2)] == '/') { // open-close tag
			std::string closedName = extractName(tag);
			std::map<std::string, std::string> closedAttrs = extractAttributes(tag);
			for (auto pair : closedAttrs) {
				modifiable = modifiable.substr(pair.first.size() + pair.second.size() + 2); // space, =, and quotes + words
			}
			std::shared_ptr<XpNode> node = std::make_shared<OpenSingleNode>(closedName, closedAttrs);
			internalXML.push_back(node);

			int offset = modifiable[closedName.size() + 3] == '>' ? 4 : 3;
			modifiable = modifiable.substr(closedName.size() + offset);
			i++;
		} else { // find the matching tag and segment everything off inside
			int matchingIndex = -1;
			std::string name = extractName(matches[i]);
			for (int j = i + 1; j < matches.size(); j++) {
				if (matches[j] == "</" + name + ">") {
					matchingIndex = j;
					break;
				}
			}
			if (matchingIndex != -1) { // found a closing tag
				std::shared_ptr<XpNode> node = constructTagNode(matches, i, matchingIndex, modifiable);
				internalXML.push_back(node);
				i = matchingIndex + 1;
			}
			else { // this is a lone opening tag
				std::string openName = extractName(tag);
				std::map<std::string, std::string> openAttrs = extractAttributes(tag);
				for (auto pair : openAttrs) {
					modifiable = modifiable.substr(pair.first.size() + pair.second.size() + 2); // space, =, and words
				}
				std::shared_ptr<XpNode> node = std::make_shared<OpenNode>(openName, openAttrs);
				internalXML.push_back(node);

				modifiable = modifiable.substr(openName.size() + 2);
				i++;
			}
		}
	}

	std::string tag = matches.at(closeIndex);
	int index = modifiable.find(tag);

	// If there's between this and the closing tag, make a byte node with it
	if (index > 0) {
		std::string prefix = modifiable.substr(0, index);
		std::shared_ptr<XpNode> node = std::make_shared<ByteNode>(prefix);
		internalXML.push_back(node);
		modifiable = modifiable.substr(index);
	}

	modifiable = modifiable.substr(name.length() + 3); // get rid of the closing tag.
	std::shared_ptr<XpNode> out = std::make_shared<TagNode>(name, attrs, internalXML);
	return out;
}

Radamsa::XpUtils::XpUtils() {}

void Radamsa::XpUtils::XpProcess(std::string input, std::vector<std::shared_ptr<XpNode>>& nodes, std::vector<Tag>& tags)
{
	std::string modifiable = std::string(input);
	std::regex tag("(<[^?])[^<]*(>)"); // get all tags that aren't the version tag and don't include <thing>text</thing> as one "tag"
	std::smatch m;
	std::regex_search(input, m, tag);

	auto tagsBegin = std::sregex_iterator(input.begin(), input.end(), tag);
	auto tagsEnd = std::sregex_iterator();

	std::vector<std::string> matches;

	for (std::sregex_iterator i = tagsBegin; i != tagsEnd; ++i) {
		std::smatch match = *i;
		matches.push_back(match.str());
	}

	for (int i = 0; i < matches.size(); i++) {
		std::string tag = matches.at(i);
		int index = modifiable.find(tag);

		// If there's anything preceding this, there's junk data in there.
		if (index > 0) {
			std::string prefix = modifiable.substr(0, index);
			std::shared_ptr<XpNode> node = std::make_shared<ByteNode>(prefix);
			nodes.push_back(node);
			modifiable = modifiable.substr(index);
		}

		if (tag[1] == '/') { // closing tag before an opening tag. Hm.
			std::string closedName = extractName(tag);
			std::shared_ptr<XpNode> node = std::make_shared<CloseNode>(closedName);
			nodes.push_back(node);

			modifiable = modifiable.substr(closedName.size() + 3);
			i++;
		}
		else if (tag[(tag.length() - 2)] == '/') { // open-close tag
			std::string closedName = extractName(tag);
			std::map<std::string, std::string> closedAttrs = extractAttributes(tag);
			for (auto pair : closedAttrs) {
				modifiable = modifiable.substr(pair.first.size() + pair.second.size() + 2); // space, =, and quotes + words
			}
			std::shared_ptr<XpNode> node = std::make_shared<OpenSingleNode>(closedName, closedAttrs);
			nodes.push_back(node);

			int offset = modifiable[closedName.size() + 3] == '>' ? 4 : 3;
			modifiable = modifiable.substr(closedName.size() + offset);
			i++;
		}
		else { // find the matching tag and segment everything off inside
			int matchingIndex = -1;
			std::string name = extractName(matches[i]);
			for (int j = i + 1; j < matches.size(); j++) {
				if (matches[j] == "</" + name + ">") {
					matchingIndex = j;
					break;
				}
			}
			if (matchingIndex != -1) { // found a closing tag
				std::shared_ptr<XpNode> node = constructTagNode(matches, i, matchingIndex, modifiable);
				nodes.push_back(node);
				i = matchingIndex + 1;
			}
			else { // this is a lone opening tag
				std::string openName = extractName(tag);
				std::map<std::string, std::string> openAttrs = extractAttributes(tag);
				for (auto pair : openAttrs) {
					modifiable = modifiable.substr(pair.first.size() + pair.second.size() + 2); // space, =, and words
				}
				std::shared_ptr<XpNode> node = std::make_shared<OpenNode>(openName, openAttrs);
				nodes.push_back(node);

				modifiable = modifiable.substr(openName.size() + 2);
				i++;
			}
		}
	}

	if (modifiable.size() > 0) { // push the rest of it as a byte node
		std::shared_ptr<XpNode> node = std::make_shared<ByteNode>(modifiable);
		nodes.push_back(node);
	}
	 // populate Tags
	for (std::string match : matches) {
		std::string name = extractName(match);
		std::map<std::string, std::string> attrs = extractAttributes(match);

		bool exists = false;
		for (auto tag : tags) {
			if (tag.name == name) exists = true;
		}

		if (!exists) {
			std::string type;
			if (match[match.length() - 2] == '/') type = "open-single";
			else {
				int matchingIndex = -1;
				for (int j = 0; j < matches.size(); j++) {
					if (matches[j] == "</" + name + ">") {
						matchingIndex = j;
						break;
					}
				}

				if (matchingIndex == -1) {
					type = "open-only";
				}
				else {
					type = "open-full";
				}
			}

			tags.push_back(Tag(name, type, attrs));
		}
	}
}

bool Radamsa::XpUtils::isTaggy(std::vector<std::shared_ptr<XpNode>> nodes)
{
	if (nodes.size() == 1 && nodes[0]->getType() == Radamsa::NodeType::Tag) return true;
	// else if length is greater than one, there must be a tag because adjacent byte sequences are merged into one node.
	return nodes.size() > 1;
}

std::string Radamsa::XpUtils::nodesToString(std::vector<std::shared_ptr<XpNode>> nodes)
{
	std::string out = "";

	for (auto node : nodes) {
		out += node->toString();
	}

	return out;
}
