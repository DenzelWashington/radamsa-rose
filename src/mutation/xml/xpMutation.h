#pragma once
#include "../mutation.h"

namespace Radamsa {
	class XpMutation : public Radamsa::Mutation {
	private:
		int delta;
		Random* random;
		struct radamsa_constants constants;
	public:
		XpMutation(int pri, struct radamsa_constants constants, Random& random);
		std::string mutate(std::string input);
		int nextDelta();
	};
}