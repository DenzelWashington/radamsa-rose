#pragma once
#include "pattern.h"

namespace Radamsa {
	class ManyMutationsPattern : public Pattern {
	public:
		ManyMutationsPattern(int pri, struct radamsa_constants constants, Random& random);
	protected:
		std::deque<std::string> applyPattern(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) override;
	};
}