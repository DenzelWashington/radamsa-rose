#include "pattern_adapter.h"
#include "chunk_queue_interface.h"
#include <stdexcept>

Radamsa::PatternAdapter::PatternAdapter(struct input_chunk* (*runFunc)(struct input_chunk*, radamsa_mutation, struct radamsa_constants, radamsa_random), int pri, struct radamsa_constants constants, Random& random, std::string name) :
    Pattern(pri, constants, random, name) {
        this->runFunc = runFunc;
}

std::deque<std::string> Radamsa::PatternAdapter::run(std::deque<std::string> inputChunks, Radamsa::Mutation *mutation) {
    struct input_chunk* head = toLinkedStruct(inputChunks);
    head = runFunc(head, mutation, this->constants, this->random);
    return fromLinkedStruct(head);
}

std::deque<std::string> Radamsa::PatternAdapter::applyPattern(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) {
    throw std::logic_error("Apply pattern called in adater");
}
