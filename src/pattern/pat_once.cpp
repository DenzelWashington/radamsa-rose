#include "pat_once.h"

/// MutateOncePattern
///
/// @param pri int repesents priority
/// @param constants radamsa_constants object
/// @param random a random object


Radamsa::MutateOncePattern::MutateOncePattern(int pri, radamsa_constants constants, Random& random) : Pattern(pri, constants, random, "od") {}


/// MutateOncePattern just return the inputChunks,
/// because mutation has already been applied, so nothing to do with the remaining chunks.
///
/// @param inputChunks queue of string
/// @param mutation radamsa mutation pointer

std::deque<std::string> Radamsa::MutateOncePattern::applyPattern(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) {
	return inputChunks; // Mutation has already been applied, so nothing to do with the remaining chunks
}
