#include "random/random.h"
#include "radamsa-rose/constants.h"
#include <vector>

namespace Radamsa {
    std::vector<char> fuse(std::vector<char> listOne, std::vector<char> listTwo, Random& random, const radamsa_constants constants);
}