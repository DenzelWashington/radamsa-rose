#include "bignum.h"
#include <stdint.h>
#include <vector>
#include <algorithm>
#include <cctype>
#include <math.h>


Radamsa::Bignum::Bignum() {
	initPowers();
	bitVector = std::vector<uint8_t>();
	fixVector = std::vector<uint32_t>();
}

Radamsa::Bignum::Bignum(const Bignum & b)
{
	initPowers();
	// [lsd ... msd]
	bitVector = std::vector<uint8_t>(b.bitVector);
	regenerateFixVector();
}

// IMPORTANT: bitvector needs to be [lsd ... msd]
Radamsa::Bignum::Bignum(std::vector<uint8_t> bits) {
	initPowers();
	bitVector = bits;
	trimLeadingZeroes();

	regenerateFixVector();
}

Radamsa::Bignum::Bignum(unsigned long long val)
{
	initPowers();
	toBitVector(val);
	regenerateFixVector();
}

Radamsa::Bignum::Bignum(std::string number) {
	Bignum base(0);
	initPowers();
	for (char const &c: number) {
		if (isdigit(c)) {
			int digit = c - '0';
			base = base * Bignum(10);
			base = base + Bignum(digit);
		}
	}
	this->bitVector = base.bitVector;
	this->regenerateFixVector();
}

void Radamsa::Bignum::initPowers() {
	powersOfTwo = std::vector<std::string>();
	powersOfTwo.push_back("1");
	powersOfTwo.push_back("2"); 
	powersOfTwo.push_back("4");
	powersOfTwo.push_back("8");
}

void Radamsa::Bignum::generatePowersTo(int cap)
{
	auto powers = powersOfTwo;
	int i = powersOfTwo.size() - 1;
	for (; i <= cap; i++) {
		std::string next = addStrings(powersOfTwo[i], powersOfTwo[i]);
		powersOfTwo.push_back(next);
	}
}

std::string Radamsa::Bignum::getNthPowerOfTwo(int n) {
	generatePowersTo(n);
	std::string out = powersOfTwo[n];
	std::reverse(out.begin(), out.end());
	return out;
}

uint32_t Radamsa::Bignum::pop()
{
	uint32_t value = peek();
	fixVector.pop_back();
	refreshBitVector();
	return value;
}

void Radamsa::Bignum::push(uint32_t fixnum)
{
	fixVector.push_back(fixnum);
	refreshBitVector();
}

uint32_t Radamsa::Bignum::peek()
{
	return fixVector[fixVector.size() - 1];
}

bool Radamsa::Bignum::empty()
{
	return fixVector.size() == 0;
}

bool Radamsa::Bignum::equals(Bignum other)
{
	return false;
}

unsigned long long Radamsa::Bignum::toInt()
{
	return (unsigned long long) *this;
}

Radamsa::Bignum Radamsa::Bignum::operator+(Bignum const & num)
{
	std::vector<uint8_t> result = std::vector<uint8_t>(bitVector);

	// Addition operates in lsd...msd resulting in this tomfoolery
	additionHelper(0, &result, num.bitVector);

	return Bignum(result);
}

Radamsa::Bignum Radamsa::Bignum::operator+(unsigned int const & num)
{
	std::vector<uint8_t> valVector = Bignum(num).bitVector;
	std::vector<uint8_t> result = std::vector<uint8_t>(bitVector);

	additionHelper(0, &result, valVector);

	return Bignum(result);
}

Radamsa::Bignum Radamsa::Bignum::operator*(Bignum const & num)
{
	std::vector<uint8_t> product = std::vector<uint8_t>();

	// Long multiplication using bit shifts
	// bitVector is [lsd ... msd]
	for (int i = 0; i < num.bitVector.size(); i++) {
		if (num.bitVector.at(i) == 1) {
			int shift = i;
			additionHelper(shift, &product, bitVector);
		}
	}

	return Bignum(product);
}

//Radamsa::Bignum& Radamsa::Bignum::operator+=(Bignum const & num)
//{
//	additionHelper(0, &bitVector, num.bitVector);
//	regenerateFixVector();
//	return *this;
//}
//
//Radamsa::Bignum& Radamsa::Bignum::operator+=(unsigned long long & num)
//{
//	std::vector<uint8_t> numVector = Bignum(num).bitVector;
//
//	additionHelper(0, &bitVector, numVector);
//	regenerateFixVector();
//	return *this;
//}

Radamsa::Bignum::operator unsigned long long() const
{
	unsigned long long acc = 0;

	for (int i = 0; i < bitVector.size(); i++) {
		acc += bitVector.at(i) * pow(2, i);
	}

	return acc;
}

void Radamsa::Bignum::operator<<(unsigned long long bits)
{
	std::reverse(bitVector.begin(), bitVector.end());
	for (int i = bits; i > 0; i--) bitVector.push_back(0);
	std::reverse(bitVector.begin(), bitVector.end());
}

Radamsa::Bignum Radamsa::Bignum::operator|(Bignum const& num)
{
	std::vector<uint8_t> newBits;

	int i = 0;
	for (; i < bitVector.size() && i < num.bitVector.size(); i++) {
		if (bitVector[i] == 1 || num.bitVector[i] == 1) {
			newBits.push_back(1);
		}
		else {
			newBits.push_back(0);
		}
	}

	if (num.bitVector.size() == bitVector.size()) return Bignum(newBits);

	auto longer = num.bitVector.size() >= bitVector.size() ? num.bitVector : bitVector;

	for (; i < longer.size(); i++) newBits.push_back(longer[i]);

	return Bignum(newBits);
}

bool Radamsa::Bignum::operator==(Bignum const & num)
{
	// Treat like a stack
	if (fixVector.size() != num.fixVector.size()) return false;
	for (int i = 0; i < fixVector.size(); i++) {
		if (fixVector.at(i) != num.fixVector.at(i)) return false;
	}

	return true;
}

bool Radamsa::Bignum::operator!=(Bignum const & num)
{
	// Treat like a stack
	if (fixVector.size() != num.fixVector.size()) return true;
	for (int i = 0; i < fixVector.size(); i++) {
		if (fixVector.at(i) != num.fixVector.at(i)) return true;
	}

	return false;
}

bool Radamsa::Bignum::operator>(Bignum const & num)
{
	if (bitVector.size() > num.bitVector.size()) return true;
	if (bitVector.size() < num.bitVector.size()) return false;

	if (bitVector.size() == num.bitVector.size()) {
		for (int i = 0; i < bitVector.size(); i++) {
			if (bitVector.at(i) == 1 && num.bitVector.at(i) == 0) return true;
			if (bitVector.at(i) == 0 && num.bitVector.at(i) == 1) return false;
		}
	}

	return false; // The numbers are equal
}

bool Radamsa::Bignum::operator<(Bignum const & num)
{
	if (bitVector.size() < num.bitVector.size()) return true;
	if (bitVector.size() > num.bitVector.size()) return false;

	if (bitVector.size() == num.bitVector.size()) {
		for (int i = 0; i < bitVector.size(); i++) {
			if (bitVector.at(i) == 0 && num.bitVector.at(i) == 1) return true;
			if (bitVector.at(i) == 1 && num.bitVector.at(i) == 0) return false;
		}
	}

	return false; // The numbers are equal
}

bool Radamsa::Bignum::operator>=(Bignum const & num)
{
	return (*this == num) || (*this > num);
}

bool Radamsa::Bignum::operator<=(Bignum const & num)
{
	return (*this == num) || (*this < num);
}

// adds the value vector to the accumulator shifted by the appropriate amount for the power of 2 multiplied by
// Works correctly only if the bit vectors are [lsd...msd]
void Radamsa::Bignum::additionHelper(int shift, std::vector<uint8_t> *acc, std::vector<uint8_t> values)
{
	int index = shift;

	int carry = 0;
	for (int i = 0; i < values.size(); i++) {
		while (index >= acc->size()) acc->push_back(0);
		
		additionReturn additionResult = binaryAdd(acc->at(index), values.at(i), carry);
		acc->at(index) = additionResult.r;
		carry = additionResult.co;
		
		index++;
	}

	while (carry == 1) { // && (index >= productAcc.size) // This should always be the case ? but I'm not checking right now so some bugs may crop up from this
		if (index >= acc->size()) {
			acc->push_back(carry);
			carry = 0;
		}
		else {
			additionReturn additionResult = binaryAdd(acc->at(index), 0, carry);
			acc->at(index) = additionResult.r;
			carry = additionResult.co;
			index++;
		}
	}
}

Radamsa::Bignum::additionReturn Radamsa::Bignum::binaryAdd(int a, int b, int ci)
{
	additionReturn toReturn;
	switch (a + b + ci) {
	case 0:
		toReturn.r = 0;
		toReturn.co = 0;
		break;
	case 1:
		toReturn.r = 1;
		toReturn.co = 0;
		break;
	case 2:
		toReturn.r = 0;
		toReturn.co = 1;
		break;
	case 3:
		toReturn.r = 1;
		toReturn.co = 1;
		break;
	default:
		toReturn.r = 0;
		toReturn.co = 0;
		break;
	}

	return toReturn;
}

void Radamsa::Bignum::regenerateFixVector()
{
	fixVector = std::vector<uint32_t>();
	std::vector<uint8_t> bits = std::vector<uint8_t>(bitVector);
	std::reverse(bits.begin(), bits.end()); // [msd ... lsd]

	while (bits.size() > 0) {
		uint32_t acc = 0;
		// Grab a fixnum (24 bits)
		for (int i = 0; i < 24 && bits.size() > 0; i++) {
			// Grab the next lsd and pop it on the front of this fixnum
			acc += bits.at(bits.size() - 1) * pow(2, i);
			bits.pop_back();
		}
		fixVector.push_back(acc); // [lsfx ... msfx]
		acc = 0;
	}

	std::reverse(fixVector.begin(), fixVector.end());
}

Radamsa::Bignum::additionReturn Radamsa::Bignum::decimalAdd(int a, int b, int ci)
{
	int sum = a + b + ci;
	additionReturn toReturn;
	toReturn.r = sum % 10;
	toReturn.co = sum / 10;
	return toReturn;
}

std::string Radamsa::Bignum::addStrings(std::string a, std::string b)
{
	std::string out;
	std::string longer;
	std::string shorter;
	int top = 0;
	if (a.size() >= b.size()) {
		longer = a;
		shorter = b;

		top = b.size();
	}
	else {
		longer = b;
		shorter = a;

		top = a.size();
	}

	int ci = 0;
	for (int i = 0; i < top; i++) {
		auto sum = decimalAdd(longer[i] - '0', shorter[i] - '0', ci);
		out.push_back(sum.r + '0');
		ci = sum.co;
	}

	for (int i = top; i < longer.size(); i++) {
		auto sum = decimalAdd(longer[i] - '0', 0, ci);
		out.push_back(sum.r + '0');
		ci = sum.co;
	}

	if (ci > 0) out.push_back(ci + '0');

	return out;
}

void Radamsa::Bignum::trimLeadingZeroes()
{
	if (bitVector.size() == 0) return;

	if (bitVector.at(bitVector.size() - 1) == 0) {
		bitVector.pop_back();
		trimLeadingZeroes();
	}

	return;
}

void Radamsa::Bignum::refreshBitVector() {
	bitVector = std::vector<uint8_t>();

	// construct as [msd ... lsd]
	for (int i = 0; i < fixVector.size(); i++) {
		uint32_t temp = fixVector.at(i);

		for (int j = 23; j >= 0; j--) {
			if (temp - pow(2, j) >= 0) {
				temp = temp - pow(2, j);
				bitVector.push_back(1);
			}
			else {
				bitVector.push_back(0);
			}
		}
	}

	// Convert to [lsd ... msd]
	std::reverse(bitVector.begin(), bitVector.end());
	trimLeadingZeroes();
}

std::string Radamsa::Bignum::toString()
{
	std::string out;

	for (int i = 0; i < bitVector.size(); i++) {
		if (bitVector[i] == 1) {
			generatePowersTo(i);
			out = addStrings(out, powersOfTwo[i]);
		}
	}

	std::reverse(out.begin(), out.end());
	return out;
}

void Radamsa::Bignum::toBitVector(unsigned long long val)
{
	unsigned long long temp = val;
	bitVector = std::vector<uint8_t>();

	// construct as [msd ... lsd]
	for (int i = 63; i >= 0; i--) {
		if ((temp - pow(2, i)) >= 0) {
			temp = temp - pow(2, i);
			bitVector.push_back(1);
		}
		else {
			bitVector.push_back(0);
		}
	}

	// Convert to [lsd ... msd]
	std::reverse(bitVector.begin(), bitVector.end());
	trimLeadingZeroes();
}
