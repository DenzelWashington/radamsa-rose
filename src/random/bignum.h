#pragma once
#include <vector>
#include <string>
#include <stdint.h>

namespace Radamsa {
	class Bignum {
	public:
		// This is a dreadfully inefficient hack done at the end of the project
		// Please replace my janky toString stuff with a better bignum library in the future
		std::vector<std::string> powersOfTwo;

		typedef struct {
			int r;
			int co;
		} additionReturn;
		// [lsd ... msd] - very helpful for arithmetic.
		std::vector<uint8_t> bitVector;
		// [msfx ... lsfx]
		std::vector<uint32_t> fixVector;

		Bignum();
		Bignum(const Bignum &b);
		Bignum(std::vector<uint8_t> bits);
		Bignum(unsigned long long val);
		Bignum(std::string number);
		void initPowers();
		void generatePowersTo(int cap);

		// Uncons
		uint32_t pop();
		// Cons
		void push(uint32_t fixnum);
		// Useful for getting the first fixnum
		uint32_t peek();
		// Checked at least once in random, so we need it.
		bool empty();
		bool equals(Bignum other);

		unsigned long long toInt();

		// ------------------ Operators ----------------------
		// Arithmetic
		Bignum operator + (Bignum const &num);
		Bignum operator + (unsigned int const &num);
		Bignum operator * (Bignum const &num);

		// Assignment
		//Bignum& operator += (Bignum const &num);
		//Bignum& operator += (unsigned long long &num);

		// Conversion
		operator unsigned long long() const;

		// Bitwise
		void operator << (unsigned long long bits);
		Bignum operator | (Bignum const& num);

		// Comparison
		bool operator == (Bignum const &num);
		bool operator != (Bignum const &num);
		bool operator > (Bignum const &num);
		bool operator < (Bignum const &num);
		bool operator >= (Bignum const &num);
		bool operator <= (Bignum const &num);

		// This should be private but some spaghetti code needs it
		void refreshBitVector();

		std::string toString();
	// public for testing 
		//test function
		std::string getNthPowerOfTwo(int n);
	// private:
		void regenerateFixVector();
		std::string addStrings(std::string a, std::string b);
		additionReturn decimalAdd(int a, int b, int ci);
		void additionHelper(int shift, std::vector<uint8_t> *acc, std::vector<uint8_t> values);
		additionReturn binaryAdd(int a, int b, int ci);
		void trimLeadingZeroes();
		// [lsd ... msd]
		void toBitVector(unsigned long long val);
	};
}
