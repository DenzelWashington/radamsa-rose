#include "radamsa-rose/rose-fuzzer.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/// example C code to construct radamsa-rose fuzzer
/// and use the shared library


int main(int argc, const char* argv[]) {
	struct radamsa_constants constants = { 0 };
	initDefaultConstants(&constants);

	int seed = atoi(argv[1]);
	radamsa_rose fuzzer = newRadamsaRoseFuzzer(seed, constants);

	char out[1024];
	char in[1024];
	if (argc < 3) {
		fgets(in, 1024, stdin);
	} else {
		strncpy(in, argv[2], 1024);
	}

	size_t len = runRadamsa(fuzzer, in, strlen(in), out, 1024);
	out[len] = '\0';
	printf("%s\n", out);
	deleteRadamsaRoseFuzzer(fuzzer);

	return 0;
}