Example execution of string->mutators

(string->mutators "ft=2,fo=2,fn,num=5,td,tr2,ts1,tr,ts2,ld,lds,lr2,li,ls,lp,lr,lis,lrs,sr,sd,bd,bf,bi,br,bp,bei,bed,ber,uw,ui=2,xp=9,ab")

Splits the input on "," then "=" 

ps = (("ft" "2") ("fo" "2") ("fn") ...) 

Turn priority strings to numbers and give priority 1 (lowest) to non-prioritized mutations 

ps = (("ft" . 2) ("fo" . 2) ("fn" . 1) ...)

fs = ((tuple 10 (2) sed-fuse-this ("ft")) (tuple 10 (2) sed-fuse-old ("fo")) (tuple 10 (1) sed-fuse-next ("fn")) ...) -- 10 is max-score, a constant in the radamsa algorithm

If every single mutation specified is real, return fs, otherwise false

---

mutators->mutator :: rs ((tuple max-score (priority) mutation-function (name) ...) -> 

The tuples in the input list are referred to as nodes in the code.
Out begins as an empty list.
While mutas is not null (empty), add a set containing {the first tuple of the list, the number 1, and a random number between 2 and max-score} to out.

If mutas is null, the program calls stderr-probe with a sorted list by priority times max-score of mutation names and priorities/scores, and then returns the random seed and a continuation of mux-fuzzers on out

mux-fuzzers 