# check if the input is pcapng type
pcapng-input?(dict):
    return "pca-png" == dict[generators].name

# Iterate through the mutation list, call instrument everytime to get a list of actual callable mutation
pcapng-instrument-mutations(mutations[], val, priority, function, name):

    # decorate the actual mutation
    define instrument(mutation):
        define InstrumentedMutation(rs, ll, meta): #DECORATOR
            currentBlock <- list(ll[i])
            currentBlockType <- currentBlock[0:3]
            nextBlocks <- list(ll[i+1])
            switch (currentBlockType):
                0x0A0D0D0A -> NOP Mutation
                0x00000001 -> NOP Mutation
                0x00000002 -> NOP Mutation
                0x00000003 -> NOP Mutation
                0x00000004 -> NOP Mutation
                0x00000005 -> NOP Mutation
                0x00000006 -> 
                    originalPacket <- list->bytevector(extractEnhancedPacket(currentBlock))
                    f, rs, ll, meta, d <- mutation(rs, list(originalPacket), meta)
                    mutatedPackets <- list(ll)
                    mutatedBlocks <- list->bytevector(new-enhanced-block(currentBlock, mutatedPackets))
                else:
                    NOP Mutations
    return InstrumentedMutation

    for mutation in mutations[]:
        instrumentedMutations[i] <- instrument(mutation)
return instrumentMutations[]


extractEnhancedPacket():
    return parseEnhancedBlock.packet-data

# parse the block, get data like packet length and block type from the block
parseEnhancedBlock(block):
    packetLength <- block[20:23]
    paddingLength <- 4 - (packetLength % 4)
    packetLengthWithPadding <- packetLength + paddingLength
    optionsLength <- blockLength - (28 + packetLengthWithPadding + 4)

    (list (cons 'block-type               (pick 0  4 block))
          (cons 'first-block-total-length (bytes->uint32 (pick 4 4 block)))
          (cons 'interface-id             (pick 8  4 block))
          (cons 'timestamp-high           (pick 12 4 block))
          (cons 'timestamp-low            (pick 16 4 block))
          (cons 'captured-packet-length   (pick 20 4 block))
          (cons 'original-packet-length   (pick 24 4 block))
          (cons 'packet-data              (pick 28 (+ packet-length) block))
          (cons 'options                  (pick (+ 28 packet-length-with-padding) options-length block))
          (cons 'last-block-total-length  (bytes->uint32 (pick (+ 28 packet-length-with-padding options-length) 4 block))))))


# return the default checksummer    
dummyCheckSummer(cs, ll):
    return ll, cs, "n/a"

# Call hash to get the check sum trits, if the trit is in the check summer, then get the value, if not, put the trit into the check summer
checkSummer(cs, ll):
    lst <- force-ll(ll) #turn ll into a list                                                            
    bs <- outputStream2ByteStrem(lst) # outputStream2ByteStream returns (lst[0][0]...lst[0][n-1], #procedure) -> lst[0][0]...lst[0][n-1]# , lst[1][0]...lst[1][n-1], #procedure

    csumTrits, csumString <- hash(bs)
    if dget(cs, csumTrit):
        return lst, cs, False
    else:
        return lst, dput(cs, csumTrits), csumString


# Apply the function on every element in the list start from the right end. if the node length is not 0, call streamChunk on the node
outputStream2ByteStream(lst):
    foldr function(node = null, tl = lst):
        if node is vector:
            return function:
                len <- length(node)
                if len == 0
                    return tl
                else:
                    return streamChunk(node,(len-1),tl)
        else:
            return tl


StreamChunk(buff, pos, tail):
    while pos >= 0:
        tail.push(buff[pos])
        pos--
    end

# use random to get score for each mutation, then pass the mutation list to the muxfuzzers for generating and calling mutation functions
mutators2Mutator(rs, mutas[]):
    while (mutas is not null):
        rs, n <- rand(rs, maxScore)
        mutas[i].score = max(2,n)
        out.push(mutas[i])
    end

    return muxFuzzers(out)

# THE METHOD ACTUALLY CALLS MUTATIONS. First call weightedPermutations to get a list of mutation object sorted by score*priority, 
# then for each element in the list of mutation, get data like score and priority, including the mutation function pointer. Then 
# use the function pointer to call mutate. After the mutate ends, calls the adjustPriority function to update the priority for future use.
muxFuzzers(fs[]): 
    if (ll is a pair):
        pfs <- weightedPermutations(rs, fs)  #sort fs by rand(score * priority)
        while pfs is not Empty:
            node <- pfs.pop()
            mscore <- node.score
            mpri <- node.pri
            wtfuzz <- node.fn-pointer
            mname <- node.name
            mfn, rs, mll, meta, delta <- wtfuzz(rs,ll,meta) #return mutation, rs, mutated ll, new meta, delta
            mutation.adjustPriority(mscore, delta) #priority is actually score
            out.push(mutation)
            if((mll is not a linked list of bytes) or ll != mll):
                return mutationManager(), rs, mll, meta  # updated scored mutations (out in radamsa), new rs, mll, new meta
        end while
        return changed mutationm list, new rs, ll, new meta
    else if (ll is null):
        do nothing and return
    else:
        find data in ll or hit end of ll

# Preperation for muxfuzzers, use random to calculate score*priority, then sort the mutation list based on the number so that the first one has the highest priority.
weightedPermutation(fs[]):
    pass in (score, priority, function, name)
    return sort(rand(score*priority))

# For every element in the priority list, call priority2generator to get the generator object, if the object is valid, call muxGenerators.
generatorPriority2generator(rs, pris, args, fail, n):
    gs <- for each pri in pris:
        gens.add(priority2generator(pri))
        return gens
    remove # false from gs  #gs is now only real generator
    if(gs is empty) then fail("no generators")
    if(size of gs == 1) return gs[0]
    else:
        return muxGenerators(gs)

# Based on arg string, determine type of streamer and return the corresponding generator.
priority2generator(rs, args, fail, pri):
    paths <- filter on args not a "-"
    paths <- if paths==null then #false, else list2vector(paths)
    if pri:
        name <- pri.name
        priority <- pri.priority
        switch (name):
            case stdin:
                if "-" found in args:
                    return new stdinGenerator(priority, rs, n==1)
                else:
                    return #false
            case file: 
                return new fileStreamer(priority, paths) if paths != null else #false
            case pcapng:
                return new pcapngStreamer(priority, paths) if paths != null else #false
            case jump:
                return new JumpStreamer(priority, paths) if paths != null else #false
            case random:
                return new RandomGenerator(priority)
            default:
                unknown generator
        fail("bad gen priority")

# Like the muxFuzzers, first sort the generator list by priority, then sum the priorities and call choose-pri to get generators.
muxGenerators(gs):
    gs <- sort gs on priority
    n <- sum priorities in gs
    define gen:
        rs, n <- rand(rs, n)
        return choose-pri(gs, n)  #choose a generator that takes in rs
    return gen


choose-pri(l, n): # l is gs
    for i in l:
        this <- i.priority
        if n < this:
            return i.generator
        n -= this

# return the default empty Digest object, which is a tree in Radamsa
emptyDigest(max):
    return (empty, max, 0)


muxPatterns(patterns):
    same as muxGenerators for pattern priorities
    return pattern(rs, ll, muta, meta) # picks a pattern and calls it


output(output-lst, fd):
    if(udp):
        # junk
    else:
        output2fd(output, fd)

output2fd(ll, fd):
    ll, n <- write ll to file, fd  # n bytes written
    lst <- force-ll(ll)
    state <- llast(ll)
    rs, muta, meta <- state
    if(not stdout) :
        then close fd
    return rs, muta, meta, n

# Takes input string, first parse it and call selection2priority to get the priority value, then use the priority to call priority2fuzzer
# to get the mutation data model used in muxfuzzers
string2mutators(str):
    ps <- str.split(',').split('=')
    ps <- foreach p in ps:
        selection2priority(p)
    fs <- for each p in ps:
        priority2fuzzer(p)
    if fs contains no false:
        return fs
    else:
        return false

# Parse the input list to priority value
selection2priority(lst):
    l <- length(lst)
    if(l == 2):
        pri <- string2number(pri-string)
        if(not pri):
            print "Bad Priority: " + pri
        else if(pri < 0):
            print "Inconceivable: " + pri
        else:
            return (name pri)
    else if(l == 1):
        return (name, 1)
    else:
        print "too many things"
    return false

# use data in node like priority and name to create a mutation model
priority2fuzzer(node):
    if not node return false
    # cond => proc: if cond, proc(cond)
    if(name2mutation(node.name)):
        return (max-score, pri, mutationFunction, name) #max-score is constant 10, this line builds (score, pri, func, name)
    else:
        return false

# get the mutation function based on the string
name2mutation(name):
    return function pointer of mutation "str"(name)


# like string2mutators, parse the input string to get patterns
string2patterns(str):
    ps <- str.split(',').split('=')
    ps <- for each p in ps:
            selection2priority(p)
    ps <- for each p in ps:
            priority2patterns(p)
    if ps contains no false:
        return ps
    else:
        return false

priority2patterns(pri):
    func <- choose pattern from patterns where pri.name #like name2mutation
    if func:
        return (pri, func)
    else:
        print err
        return false

string2Count(str):
    if(str in {"inf","infinity", "-1", "forever"}):
        return Infinity
    else if(str is a number > 0):
        return str.toInt()
    else:
        return false

string2Natural(str):
    return false if str is not natural number else str.toInt()

# just get the priority from string
string2generatorPriorities(str):
    ps <- str.split(',').split('=')
    ps <- for each p in ps:
            selection2priority(p)
    if ps contains no false:
        return ps
    else:
        return false

string2hash(str):
    switch(str):
        case "stream":
            return HashStream
        case "sha256", "sha":
            return hash-sha256
        default:
            return false

stdinGenerator(rs, online?):
    rs, ll <- port2stream(rs, stdin)
    ll <- if online?
              ll
          else
              force-ll(ll)
    return function(rs):
        return rs, ll, meta["generator", "stdin"] #empty before generator

randomGenerator():
    rs, seed <- rand(rs, x100000000000000000000) #20 0s
    return rs, randomStream(seed2rands(seed)), meta["generator", "stdin"] #was empty

port2stream(rs, port):
    rs, seed <- rand(rs, 100000000000000000000)
    return rs, function:
        streamPort(seed2rands(seed),port)

# takes in the port, get the block and apply switch on it. Once find the target block, add the size of the block to the length 
# and continues the loop until hit the end, then recurse back and append each result to the block
streamPort(rs, port):
    rs, first <- randBlockSize(rs)
    loop (rs, last: false, wanted: first, len: 0):
        block <- readByteVector(wanted, port)
        switch(block):
            case eof:
                close port if not stdin
                if last:
                    return (last, null)
                else :
                    return null
            case not block:
                close port in not stdin
                if last:
                    return list(last)
                else:
                    return null
            case sizeb(block) == wanted:
                block <- merge(last, block)
                rs, next <- randBlockSize(rs)
                pair(block, loop(rs, false, next, (len + sizeb(block)))) #append loop result to block, making a linked list of blocks
            default:
                loop(rs, merge(last, block), wanted - sizeb(block), len)



randBlockSize(rs):
    return rs, (random block size between max and min block size)

merge(head,tail):
    if head:
        list2vector(head.append(tail))
    else:
        return tail

# One time mutation pattern, just call mutateOnce 
patOnceDec(rs, ll, mutator, meta):
    return mutateOnce(rs, ll, mutator, meta["pattern"]<-"once-dec", function(ll, rs, mutator, meta):
                                                                        append(ll, list(rs,mutator,meta)))

# many mutation pattern, possibly recursively call mutateOnce to mutate the subsequent chunks of the selected chunk
patManyDec(rs, ll, mutator, meta):
    same as patOnceDec
    func:
        rs,muta? <- rand-occurs? (rs,remutate-probability) # remutate-probability = 4/5
        if(muta?):
            pat-many-dec(rs,ll,mutator,meta)
        else:
            return lappend(ll,list(tuple(rs,mutator,meta))) #appends all mutations
        
# burst pattern, gaurantee to mutate once, and then possibly keep mutating the same chunk more times
patBurst(rs, ll, mutator, meta):
    meta["pattern"] = "burst-dec"
    same as other patterns
    func(ll, rs, mutator, meta):
        loop(rs, ll, mutator, meta, n = 1):
            rs, p <- randOccurs?(rs, remutate-probability)
            if(p or (n < 2)):
                mutator, rs, ll, meta <- mutator(rs, ll, meta)
                loop(rs, ll, mutator, meta, n+1)
            else:
                lappend(ll,list(tuple(rs,mutator,meta)))

# Process rational number
randOccurs?:
    if prob is rational:
        numerator, denominator <- prob 
        if numerator == 1 #(1/n):
            rs,n <- rand(rs,denominator)
            return rs, n==0
        else:
            rs, n <- rand(rs, denominator)
            return rs, n < numerator
    else if(prob == 0):
        return rs, true
    else:
        return rs, false

mutateOnce(rs, ll, mutator, meta, cont):
    rs, ip <- rand(rs, initial-ip) # initial inverse probability (#magic)
    this, ll <- uncons(ll, false)
    if this:
        while ll != null:
            if ll is a function:
                ll = ll.()
            else:
                rs, n <- rand(rs, ip)
                if ((n==0) or (ll == null)) and ((!using pcapngGenerator) or (pcapngBlockToMutate(this)):  
                    ll.push(this)
                    mutator,rs,ll,meta <- mutator(rs,ll,meta)
                    return pattern(ll,rs,mutator,meta)
                else if(null? ll)
                    return list(this)
                else
                    pair(this,loop(rs,car(ll),cdr(ll),ip+1))
    else:
        return pattern(null,rs,mutator,meta)

uncons(l, d):
    if pair(l):
        return l[0], sublist(l,1)
    else if l == null:
        return d, l
    else:
        return uncons(l.(), d)




skip:

file-streamer
pcapng-streamer
jump-streamer