#include "catch.hpp"
#include "generator/stdingenerator.h"

TEST_CASE("StdinGenerator can generate output with one character of input") {
	struct radamsa_constants consts = { 0 };
	consts.minBlockSize = 256;
	consts.maxBlockSize = 512;
	Radamsa::Random random(0);
	Radamsa::StdinGenerator gen(0, consts, random);

	SECTION("Test stdin gen on ascii A") {
		std::deque<std::string> output = gen.generate("A");
		REQUIRE(output.size() == std::size_t(1));
		REQUIRE(output.front() == "A");
	}

	SECTION("Test stdin gen on ascii z") {
		std::deque<std::string> output = gen.generate("z");
		REQUIRE(output.size() == std::size_t(1));
		REQUIRE(output.front() == "z");
	}

	SECTION("Test stdin gen on ascii backspace character") {
		std::deque<std::string> output = gen.generate("\x08");
		REQUIRE(output.size() == std::size_t(1));
		REQUIRE(output.front() == "\x08");
	}

	SECTION("Test stdin gen on ascii null character") {
		std::deque<std::string> output = gen.generate("\x00");
		REQUIRE(output.size() == std::size_t(1));
		REQUIRE(output.front() == "\x00");
	}
}

TEST_CASE("Test Stdin Generator on empty string") {
	struct radamsa_constants consts = { 0 };
	consts.minBlockSize = 256;
	consts.maxBlockSize = 512;
	Radamsa::Random random(0);
	Radamsa::StdinGenerator gen(0, consts, random);

	std::deque<std::string> out = gen.generate("");
	REQUIRE_FALSE(out.empty());
	REQUIRE(out.front().empty());
}

TEST_CASE("Test Stdin Generator chunks the input") {
	struct radamsa_constants consts = { 0 };
	Radamsa::Random random(0);

	SECTION("Chunk into 1 byte chunks") {
		consts.minBlockSize = 1;
		consts.maxBlockSize = 1;
		Radamsa::StdinGenerator gen(0, consts, random);
		std::deque<std::string> chunks = gen.generate("abcde");
		REQUIRE(chunks.size() == std::size_t(5));
		REQUIRE(chunks.front() == "a");
		REQUIRE(chunks.back() == "e");
	}

	SECTION("Chunk into random sized chunks") {
		consts.minBlockSize = 1;
		consts.maxBlockSize = 5;
		Radamsa::StdinGenerator gen(0, consts, random);
		std::deque<std::string> chunks = gen.generate("abcdefjhij");
		REQUIRE(chunks.size() >= std::size_t(2));
		REQUIRE(chunks.back().back() == 'j');
		
		// Assert different sized chunks
		bool sameSize = true;
		std::size_t size = chunks.front().size();
		chunks.pop_front();
		do {
			sameSize &= (size == chunks.front().size());
			size = chunks.front().size();
			chunks.pop_front();
		} while (!chunks.empty());
		REQUIRE_FALSE(sameSize);
	}
}

TEST_CASE("Test Stdin generator depends on Random") {
	struct radamsa_constants consts = { 0 };
	consts.minBlockSize = 1;
	consts.maxBlockSize = 5;

	SECTION("Seed 0") {
		Radamsa::Random random(0);
		Radamsa::StdinGenerator gen(0, consts, random);
		std::deque<std::string> chunks = gen.generate("abcdefjhijklmnop");
		REQUIRE(chunks.size() == std::size_t(7));
	}

	SECTION("Seed 1234567890") {
		Radamsa::Random random(23232323);
		Radamsa::StdinGenerator gen(0, consts, random);
		std::deque<std::string> chunks = gen.generate("abcdefjhijklmnop");
		REQUIRE(chunks.size() == std::size_t(6));
	}
}

TEST_CASE("Test how stdin generator handles erroneous constants") {
	struct radamsa_constants consts = { 0 };
	Radamsa::Random random(0);
	consts.minBlockSize = 1;
	consts.maxBlockSize = 5;

	SECTION("Minimum Block size of 0") {
		consts.minBlockSize = 0;
		Radamsa::StdinGenerator gen(0, consts, random);
		REQUIRE_THROWS(gen.generate(""));
	}

	SECTION("Maximum Block size of 0") {
		consts.maxBlockSize = 0;
		Radamsa::StdinGenerator gen(0, consts, random);
		REQUIRE_THROWS(gen.generate(""));
	}

	SECTION("Minimum Block size is greater than Maximum Block size") {
		consts.maxBlockSize = 1;
		consts.minBlockSize = 5;
		Radamsa::StdinGenerator gen(0, consts, random);
		REQUIRE_THROWS(gen.generate(""));
	}

	SECTION("Negativ Min block size") {
		consts.minBlockSize = -1;
		Radamsa::StdinGenerator gen(0, consts, random);
		REQUIRE_THROWS(gen.generate(""));
	}
	
}

TEST_CASE("Test stdin generator memoization", "[gen-stdin][gen-stdin-memo]") {
	struct radamsa_constants consts = { 0 };
	Radamsa::Random random(0);
	consts.minBlockSize = 1;
	consts.maxBlockSize = 1;

	Radamsa::StdinGenerator gen(0, consts, random);
	auto out = gen.generate("12345");
	CHECK(out.size() == 5);
	CHECK(out.front() == "1");
	CHECK(out.back() == "5");

	auto secondOut = gen.generate("a");
	REQUIRE(secondOut == out);

	secondOut = gen.generate("54321");
	REQUIRE(secondOut == out);

	secondOut = gen.generate("678910");
	REQUIRE(secondOut == out);

	secondOut = gen.generate("");
	REQUIRE(secondOut == out);
}

TEST_CASE("Test stdin generator memoization on empty string first", "[gen-stdin][gen-stdin-memo]") {
	struct radamsa_constants consts = { 0 };
	Radamsa::Random random(0);
	consts.minBlockSize = 1;
	consts.maxBlockSize = 1;

	Radamsa::StdinGenerator gen(0, consts, random);
	auto out = gen.generate("");
	CHECK(out.size() == 1);
	CHECK(out.front().empty());

	auto secondOut = gen.generate("a");
	REQUIRE(secondOut == out);

	secondOut = gen.generate("54321");
	REQUIRE(secondOut == out);

	secondOut = gen.generate("678910");
	REQUIRE(secondOut == out);

	secondOut = gen.generate("12345");
	REQUIRE(secondOut == out);
}