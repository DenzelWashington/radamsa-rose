#include "catch.hpp"
#include "enable_ordered_map.h"
#include "mutation/byte/byteDropMutation.h"
#include "mutation/byte/byteFlipMutation.h"

using namespace Radamsa;

TEST_CASE("Check insertion of enable ordered map", "[enable-order]") {
    EnableOrderedMap<Mutation> sut;
    struct radamsa_constants c { 0 };
    Random rand(0);

    SECTION("Test one element") {
        REQUIRE_NOTHROW(sut.insert<ByteDropMutation>(0, c, rand));
    }

    SECTION("Test two elements") {
        REQUIRE_NOTHROW(sut.insert<ByteDropMutation>(0, c, rand));
        REQUIRE_NOTHROW(sut.insert<ByteFlipMutation>(0, c, rand));
    }

    SECTION("Test the same element") {
        REQUIRE_NOTHROW(sut.insert<ByteDropMutation>(0, c, rand));
        REQUIRE_NOTHROW(sut.insert<ByteFlipMutation>(0, c, rand));
        REQUIRE_NOTHROW(sut.insert<ByteDropMutation>(0, c, rand));
    }
}

TEST_CASE("Test set priority", "[enable-order]") {
    EnableOrderedMap<Mutation> sut;
    struct radamsa_constants c { 0 };
    Random rand(0);

    sut.insert<ByteDropMutation>(0, c, rand);
    REQUIRE_NOTHROW(sut.setPriorityOf("bd", 1));
    REQUIRE_NOTHROW(sut.setPriorityOf("bf", 2));
}

TEST_CASE("Test enabling", "[enable-order]") {
    EnableOrderedMap<Mutation> sut;
    struct radamsa_constants c { 0 };
    Random rand(0);

    sut.insert<ByteDropMutation>(0, c, rand);
    REQUIRE(sut.isEnabled("bd"));
    REQUIRE_FALSE(sut.isEnabled("bf"));

    sut.setEnabled("bd", false);
    REQUIRE_FALSE(sut.isEnabled("bd"));

    sut.setEnabled("bd", true);
    REQUIRE(sut.isEnabled("bd"));
}

TEST_CASE("Test getting the ordered list from the map", "[enable-order][enable-order-list]") {
    EnableOrderedMap<Mutation> sut;
    struct radamsa_constants c { 0 };
    Random rand(0);

    REQUIRE_NOTHROW(sut.insert<ByteDropMutation>(0, c, rand));
    REQUIRE_NOTHROW(sut.insert<ByteFlipMutation>(0, c, rand));

    auto l = sut.getOrderedList();
    REQUIRE(l.front()->getName() == "bd");
    REQUIRE(l.back()->getName() == "bf");
}

TEST_CASE("Test enable and disable all", "[enable-order]") {
    EnableOrderedMap<Mutation> sut;
    struct radamsa_constants c { 0 };
    Random rand(0);

    REQUIRE_NOTHROW(sut.insert<ByteDropMutation>(0, c, rand));
    REQUIRE_NOTHROW(sut.insert<ByteFlipMutation>(0, c, rand));
    REQUIRE(sut.isEnabled("bd"));
    REQUIRE(sut.isEnabled("bf"));

    sut.setAllEnabled(true);
    REQUIRE(sut.isEnabled("bd"));
    REQUIRE(sut.isEnabled("bf"));

    sut.setAllEnabled(false);
    REQUIRE_FALSE(sut.isEnabled("bd"));
    REQUIRE_FALSE(sut.isEnabled("bf"));

    sut.setEnabled("bf", true);
    REQUIRE_FALSE(sut.isEnabled("bd"));
    REQUIRE(sut.isEnabled("bf"));

    sut.setAllEnabled(true);
    REQUIRE(sut.isEnabled("bd"));
    REQUIRE(sut.isEnabled("bf"));
}