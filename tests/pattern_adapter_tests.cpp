#include "catch.hpp"
#include "pattern/pattern_adapter.h"

struct input_chunk* superSwaggyCustomPattern(struct input_chunk* head, radamsa_mutation muta, struct radamsa_constants c, radamsa_random rand) {
    // struct input_chunk* selected = chooseChunk(head, c.initialIp, rand);
    // for (int i = 0; i < 3; ++i) {
    //     runRadamsaMutation(muta, selected);
    // }
    // return head;
    auto ret = head->next;
    delete[] head->chunkPointer;
    delete head;
    return ret;
}

TEST_CASE("Test that custom patterns can be adapted", "[pattern][pat-adapter]") {
    Radamsa::Random rand(0);
    radamsa_constants c { 0 };
    c.initialIp = 24;
    Radamsa::PatternAdapter pat(superSwaggyCustomPattern, 0, c, rand, "adapted");

    std::deque<std::string> in;
    in.push_back("I call");
    in.push_back("this an");
    in.push_back("absolute win");
    in = pat.run(in, 0);

    REQUIRE(in.size() == 2);
}