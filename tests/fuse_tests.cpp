#include "catch.hpp"
#include "fuse.h"
#include "mutation/fuse/fuse_this_mutation.h"
#include "mutation/fuse/fuse_next_mutation.h"
#include "mutation/fuse/fuse_old_mutation.h"

struct radamsa_constants newFuseConst() {
    struct radamsa_constants constants { 0 };
    constants.searchFuel = 100000; // fuse.scm line 16
    constants.searchStopIp = 8; // fuse.scm line 18
    return constants;
}

TEST_CASE("Test fuse with strings Weite found where random did not align", "[.fuse-function]") {
    Radamsa::Random rand(Radamsa::Bignum("5478396938"));
    std::string a_str = "fghij\n";
    std::vector<char> a(a_str.begin(), a_str.end());
    std::string b_str = "abcde\n";
    std::vector<char> b(b_str.begin(), b_str.end());

    CHECK(rand.peekState() == 16520334);
    std::vector<char> res = Radamsa::fuse(a, b, rand, newFuseConst());
    REQUIRE(rand.peekState() == 8920086);
}

const std::string fuseThisIn = "Now, this is a story all about how my life got flipped turned upside down\n";

TEST_CASE("Test fuse-this function does something", "[mutation][fuse-this]") {
    Radamsa::Random rand(0);
    Radamsa::FuseThisMutation m(2, newFuseConst(), rand);

    REQUIRE(m.mutate(fuseThisIn) == fuseThisIn); // Nothing is done in most cases of fuse-this
    REQUIRE(m.getName() == "ft");
    REQUIRE(m.priority == 2);
}

TEST_CASE("Test fuse-this on new-line bug", "[mutation][fuse-this][newline]") {
    Radamsa::Random rand(0);
    Radamsa::FuseThisMutation m(2, newFuseConst(), rand);

    SECTION("With newline") {
        REQUIRE(m.mutate("a\n") == "a\n");
        REQUIRE(m.mutate("ab\n") == "abab\n");
    }

    SECTION("Only newline") {
        REQUIRE(m.mutate("\n") == "\n");
    }

    SECTION("Without newline") {
        REQUIRE(m.mutate("a") == "a");
        REQUIRE(m.mutate("ab") == "ab");
    }
}

TEST_CASE("Test fuse-this on empty line", "[mutation][fuse-this][empty]") {
    Radamsa::Random rand(15);
    Radamsa::FuseThisMutation m(1, newFuseConst(), rand);
    REQUIRE(m.mutate("").empty());
}

TEST_CASE("Test fuse-this function does something actually interesting", "[mutation][fuse-this-interesting]") {
    Radamsa::Random rand(2);
    Radamsa::FuseThisMutation m(2, newFuseConst(), rand);

    REQUIRE(m.mutate(fuseThisIn) == "Now, this is a story all about how my life got flipped turned upot flipped turned upside down\n");
    REQUIRE(m.getName() == "ft");
    REQUIRE(m.priority == 2);
}

TEST_CASE("Test fuse-this function does not break on empty or one character strings", "[mutation][fuse-this-empty]") {
    Radamsa::Random rand(0);
    Radamsa::FuseThisMutation m(1, newFuseConst(), rand);
    REQUIRE(m.priority == 1);

    REQUIRE(m.mutate("").empty());
    REQUIRE(m.mutate("a") == "a");
}

TEST_CASE("Test fuse-next for something interesting", "[mutation][fuse-next]") {
    Radamsa::Random rand(0);
    Radamsa::FuseNextMutation m(5, newFuseConst(), rand);

    std::deque<std::string> input;
    input.push_back("abcd");
    //input.push("defg");
    m.mutate(input);
}

TEST_CASE("Test fuse-old for something interesting", "[mutation][fuse-old]") {
    Radamsa::Random rand(0);
    Radamsa::FuseOldMutation m(5, newFuseConst(), rand);

    std::deque<std::string> input;
    input.push_back("abcde");
    //input.push("defg");
    m.mutate(input);
}