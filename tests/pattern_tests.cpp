#include "catch.hpp"
#include "pattern/pat_once.h"
#include "pattern/pat_many.h"
#include "pattern/pat_burst.h"
#include "mock/mock_random.h"
#include <queue>

class MockMutation : public Radamsa::Mutation {
public:
	MockMutation() : Mutation(0, 0, "mock") {}
	std::deque<std::string> recordedInput;

	std::string mutate(std::string input) override {
		recordedInput.push_back(input);
		return input;
	}

	int nextDelta() override {
		return 0;
	}

};

struct radamsa_constants newConst() {
	struct radamsa_constants constants { 0 };
	constants.initialIp = 24;
	constants.remutateProbability.numerator = 4;
	constants.remutateProbability.denominator = 5;
	constants.maxMutations = 16;
	return constants;
}

/*
$ ol
You see a prompt.
> (import (rad patterns))
;; Library (rad patterns) added
> ;; Imported (rad patterns)
> (define meta (put empty 'generator 'stdin))
;; Defined meta
>  (define (muta rs ll meta) (print ll) (values rs ll meta 0))
;; Defined muta
> (define rs0 (seed->rands 0))
;; Defined rs0
*/

TEST_CASE("Test mutation once pattern will only perform one mutation", "[pattern, once]") {
	struct radamsa_constants constants = newConst();
	Radamsa::Random rand(0);
	MockMutation muta;
	
	Radamsa::MutateOncePattern pat(-2, constants, rand);
	CHECK(pat.getName() == "od");
	CHECK(pat.priority == -2);

	std::deque<std::string> inputChunks;
	CHECK(inputChunks.empty());

	/*
		> (pat-once-dec rs0 '() muta meta)
		'(#[(#<function> #<muta> ##(pattern once-dec generator stdin))])
	*/
	SECTION("Mutate once pattern does not break on empty input") {
		std::deque<std::string> out = pat.run(inputChunks, &muta);
		REQUIRE(out.empty());
	}

	/*
		> (pat-once-dec rs0 '(#u8(97 98 99 100 101)) muta meta)
		(this #u8(97 98 99 100 101))
		(#u8(97 98 99 100 101))
		#<function>
	*/
	inputChunks.push_back("abcde");
	SECTION("Mutate once pattern works on input with one chunk") {
		std::deque<std::string> out = pat.run(inputChunks, &muta);
		REQUIRE(out.size() == 1);
		REQUIRE(out.front() == "abcde");
		REQUIRE(muta.recordedInput.front() == out.front());
	}

	inputChunks.push_back("fghij");
	inputChunks.push_back("lmnop");
	inputChunks.push_back("qrstu");
	SECTION("Mutate once pattern will only select one chunk") {
		std::deque<std::string> out = pat.run(inputChunks, &muta);
		REQUIRE(out.size() == 4);
		REQUIRE(muta.recordedInput.size() == 1);
		REQUIRE(muta.recordedInput.front() == "fghij");
	}

	// Radamsa exactness untested
}

TEST_CASE("Test Many Mutations Pattern will perform on various chunks", "[pattern, many]") {
	struct radamsa_constants constants = newConst();
	Radamsa::Random rand(0);
	MockMutation muta;

	Radamsa::ManyMutationsPattern pat(2, constants, rand);
	CHECK(pat.priority == 2);

	std::deque<std::string> inputChunks;

	SECTION("Does not break on empty chunks") {
		std::deque<std::string> out = pat.run(inputChunks, &muta);
		REQUIRE(out.empty());
	}

	inputChunks.push_back("a");
	SECTION("Acts like a burst on one chunk") {
		std::deque<std::string> out = pat.run(inputChunks, &muta);
		REQUIRE(out.size() == 1);
		REQUIRE(out.front() == inputChunks.front());
		REQUIRE(muta.recordedInput.size() == 5);
	}

	inputChunks.push_back("b");
	inputChunks.push_back("c");
	inputChunks.push_back("d");
	inputChunks.push_back("e");
	SECTION("Chooses various chunks") {
		std::deque<std::string> out = pat.run(inputChunks, &muta);
		REQUIRE(out.size() == 5);
		REQUIRE(muta.recordedInput.size() == 5);

		bool uniform = true;
		std::string first = muta.recordedInput.front();
		muta.recordedInput.pop_front();
		while (!muta.recordedInput.empty()) {
			uniform &= (first == muta.recordedInput.front());
			muta.recordedInput.pop_front();
		}
		REQUIRE_FALSE(uniform);
	}

	// Radamsa exactness untested
}

TEST_CASE("Test Burst Mutations Pattern will always perform on one chunk", "[pattern, burst]") {
	struct radamsa_constants constants = newConst();
	Radamsa::Random rand(0);
	MockMutation muta;

	Radamsa::BurstMutationsPattern pat(3, constants, rand);
	CHECK(pat.priority == 3);

	std::deque<std::string> inputChunks;

	SECTION("Does not break on empty chunks") {
		std::deque<std::string> out = pat.run(inputChunks, &muta);
		REQUIRE(out.empty());
	}

	inputChunks.push_back("a");
	SECTION("Burst on one chunk") {
		std::deque<std::string> out = pat.run(inputChunks, &muta);
		REQUIRE(out.size() == 1);
		REQUIRE(out.front() == inputChunks.front());
		REQUIRE(muta.recordedInput.size() > 4);
	}

	inputChunks.push_back("b");
	inputChunks.push_back("c");
	inputChunks.push_back("d");
	inputChunks.push_back("e");
	SECTION("Burst on one chunk out of many") {
		std::deque<std::string> out = pat.run(inputChunks, &muta);
		REQUIRE(out.size() == 5);
		REQUIRE(muta.recordedInput.size() > 3);

		bool uniform = true;
		std::string first = muta.recordedInput.front();
		muta.recordedInput.pop_front();
		while (!muta.recordedInput.empty()) {
			uniform &= (first == muta.recordedInput.front());
			muta.recordedInput.pop_front();
		}
		REQUIRE(uniform);
	}

	// Radamsa exactness untested
}

TEST_CASE("Test invalid inverse probability", "[pattern, burst, many, once, ip]") {
	struct radamsa_constants constants = newConst();
	Radamsa::Random rand(0);
	MockMutation muta;

	std::deque<std::string> inputChunks;
	inputChunks.push_back("a");

	SECTION("IP = 0") {
		constants.initialIp = 0;

		SECTION("Pat once throws error") {
			Radamsa::MutateOncePattern patOnce(0, constants, rand);
			REQUIRE_THROWS(patOnce.run(inputChunks, &muta));
		}

		SECTION("Pat many throws error") {
			Radamsa::ManyMutationsPattern patMany(0, constants, rand);
			REQUIRE_THROWS(patMany.run(inputChunks, &muta));
		}

		SECTION("Pat burst throws error") {
			Radamsa::BurstMutationsPattern patBurst(0, constants, rand);
			REQUIRE_THROWS(patBurst.run(inputChunks, &muta));
		}
	}

	SECTION("IP < 0") {
		constants.initialIp = -3;

		SECTION("Pat once throws error") {
			Radamsa::MutateOncePattern patOnce(0, constants, rand);
			REQUIRE_THROWS(patOnce.run(inputChunks, &muta));
		}

		SECTION("Pat many throws error") {
			Radamsa::ManyMutationsPattern patMany(0, constants, rand);
			REQUIRE_THROWS(patMany.run(inputChunks, &muta));
		}

		SECTION("Pat burst throws error") {
			Radamsa::BurstMutationsPattern patBurst(0, constants, rand);
			REQUIRE_THROWS(patBurst.run(inputChunks, &muta));
		}
	}
}

TEST_CASE("Test 100% Remutation Probability", "[pattern, burst, once, many, remutate]") {
	struct radamsa_constants constants = newConst();
	constants.remutateProbability.numerator = 1;
	constants.remutateProbability.denominator = 1;
	constants.maxMutations = 16;
	Radamsa::Random rand(0);
	MockMutation muta;

	std::deque<std::string> inputChunks;
	inputChunks.push_back("a");

	SECTION("Mutate once pattern unaffected") {
		Radamsa::MutateOncePattern patOnce(0, constants, rand);
		std::deque<std::string> out = patOnce.run(inputChunks, &muta);
		REQUIRE(out.size() == 1);
		REQUIRE(muta.recordedInput.size() == 1);
	}

	SECTION("Many mutations pattern stops upon reaching the maximum number of mutations") {
		SECTION("Will stop on max number") {
			constants.maxMutations = 5;
			Radamsa::ManyMutationsPattern patMany(0, constants, rand);
			std::deque<std::string> out = patMany.run(inputChunks, &muta);
			REQUIRE(out.size() == 1);
			REQUIRE(muta.recordedInput.size() == 5);
		}

		SECTION("Will allow successive calls to max number") {
			constants.maxMutations = 15;
			Radamsa::ManyMutationsPattern patMany(0, constants, rand);
			std::deque<std::string> out = patMany.run(inputChunks, &muta);
			REQUIRE(out.size() == 1);
			REQUIRE(muta.recordedInput.size() == 15);

			out = patMany.run(inputChunks, &muta);
			REQUIRE(out.size() == 1);
			REQUIRE(muta.recordedInput.size() == 30);
		}
	}

	SECTION("Burst mutations pattern stops upon reaching the maximum number of mutations") {
		SECTION("Will stop on max number") {
			constants.maxMutations = 7;
			Radamsa::BurstMutationsPattern patBurst(0, constants, rand);
			std::deque<std::string> out = patBurst.run(inputChunks, &muta);
			REQUIRE(out.size() == 1);
			REQUIRE(muta.recordedInput.size() == 7);
		}

		SECTION("Will allow successive calls to max number") {
			constants.maxMutations = 15;
			Radamsa::BurstMutationsPattern patBurst(0, constants, rand);
			std::deque<std::string> out = patBurst.run(inputChunks, &muta);
			REQUIRE(out.size() == 1);
			REQUIRE(muta.recordedInput.size() == 15);

			out = patBurst.run(inputChunks, &muta);
			REQUIRE(out.size() == 1);
			REQUIRE(muta.recordedInput.size() == 30);
		}
	}
}

TEST_CASE("Test patterns with 1 or less max mutations", "[pattern, burst, many, once, max]") {
	struct radamsa_constants constants = newConst();
	Radamsa::Random rand(0);
	MockMutation muta;

	std::deque<std::string> inputChunks;
	inputChunks.push_back("a");
	
	SECTION("1 max mutation") {
		constants.maxMutations = 1;
		SECTION("Once") {
			Radamsa::MutateOncePattern patOnce(0, constants, rand);
			std::deque<std::string> out = patOnce.run(inputChunks, &muta);
			REQUIRE(muta.recordedInput.size() == 1);
		}

		SECTION("Many") {
			Radamsa::ManyMutationsPattern patMany(0, constants, rand);
			std::deque<std::string> out = patMany.run(inputChunks, &muta);
			REQUIRE(muta.recordedInput.size() == 1);
		}

		SECTION("Burst") {
			Radamsa::BurstMutationsPattern patBurst(0, constants, rand);
			std::deque<std::string> out = patBurst.run(inputChunks, &muta);
			REQUIRE(muta.recordedInput.size() == 1);
		}
	}

	SECTION("0 max mutations") {
		constants.maxMutations = 0;
		SECTION("Once") {
			Radamsa::MutateOncePattern patOnce(0, constants, rand);
			std::deque<std::string> out = patOnce.run(inputChunks, &muta);
			REQUIRE(muta.recordedInput.size() == 0);
		}

		SECTION("Many") {
			Radamsa::ManyMutationsPattern patMany(0, constants, rand);
			std::deque<std::string> out = patMany.run(inputChunks, &muta);
			REQUIRE(muta.recordedInput.size() == 0);
		}

		SECTION("Burst") {
			Radamsa::BurstMutationsPattern patBurst(0, constants, rand);
			std::deque<std::string> out = patBurst.run(inputChunks, &muta);
			REQUIRE(muta.recordedInput.size() == 0);
		}
	}
}

TEST_CASE("Test pattern missing chunks with one max mutation", "[pattern][pat-chunk-missing]") {
	struct radamsa_constants constants = newConst();
	Radamsa::Random rand(0);
	MockMutation muta;

	std::deque<std::string> inputChunks;
	inputChunks.push_back("one");
	inputChunks.push_back("two");
	inputChunks.push_back("tre");
	inputChunks.push_back("for");
	inputChunks.push_back("fiv");
	inputChunks.push_back("six");
	inputChunks.push_back("svn");
	inputChunks.push_back("ate");
	inputChunks.push_back("n9n");
	inputChunks.push_back("ten");
	constants.maxMutations = 1;
	Radamsa::MutateOncePattern patOnce(0, constants, rand);

	auto out = patOnce.run(inputChunks, &muta);
	REQUIRE(out.size() == 10);
}