#include "catch.hpp"
#include "mock/mock_random.h"

TEST_CASE("Test that mock random loads nextIntger correctly", "[mock, random]") {
	MockRandom rand;
	rand.expectedNext(1);
	rand.expectedNext(5);
	rand.expectedNext(10);

	REQUIRE(rand.nextInteger(20) == 1);
	REQUIRE(rand.nextInteger(30UL) == 5);
	REQUIRE(rand.nextInteger(Radamsa::Bignum(0)) == 10);
}

TEST_CASE("Test expect values for methods", "[mock, random]") {
	MockRandom rand;
	rand.expectedNext(10);
	rand.expectedNext(0);
	rand.expectedNext(5);
	rand.expectedNext(-1);
	
	struct rational rat { 1 , 1 };
	REQUIRE(rand.doesOccur(rat));
	REQUIRE_FALSE(rand.doesOccur(rat));

	REQUIRE(rand.nextBlockSize(0, 2) == 5);
	REQUIRE(rand.randomDelta() == -1);
	
	rand.expectedNext(3);
	rand.expectedNext(20);
	rand.expectedNext(-4);
	Radamsa::Bignum actual = rand.nextNBit(5);
	Radamsa::Bignum expected = Radamsa::Bignum(3);
	REQUIRE(actual == expected);
	REQUIRE(rand.nextLogInteger(10) == Radamsa::Bignum(20));
	REQUIRE(rand.nextIntegerRange(4, 5) == -4);
}

TEST_CASE("Test all expected calls assertion", "[mock, random]") {
	MockRandom rand;
	REQUIRE(rand.allExpectedCallsMade());
	
	rand.expectedNext(1);
	REQUIRE_FALSE(rand.allExpectedCallsMade());

	rand.randomDelta();
	REQUIRE(rand.allExpectedCallsMade());
}