#include "catch.hpp"
#include "../mock/mock_random.h"
#include "mutation/tree/treeStutterMutation.h"

TEST_CASE("Nested sublists stuttered") {
	std::string input = "spaghetti (mari<nara><cari>) sauce";
	MockRandom randy = MockRandom();
	Radamsa::TreeStutterMutation m = Radamsa::TreeStutterMutation(10, {}, randy);
	randy.expectedNext(0);
	randy.expectedNext(5);

	std::string out = m.mutate(input);
	REQUIRE(out == "spaghetti (mari(mari(mari(mari(mari(mari<nara><cari>)<cari>)<cari>)<cari>)<cari>)<cari>) sauce");
	REQUIRE(randy.allExpectedCallsMade()); 
}

TEST_CASE("No sublists to stutter") {
	std::string input = "spaghetti sauce";
	MockRandom randy = MockRandom();
	Radamsa::TreeStutterMutation m = Radamsa::TreeStutterMutation(10, {}, randy);

	std::string out = m.mutate(input);
	REQUIRE(out == "spaghetti sauce");
	REQUIRE(randy.allExpectedCallsMade());
}

TEST_CASE("One sublist stutter") {
	std::string input = "spaghetti sauce";
	MockRandom randy = MockRandom();
	Radamsa::TreeStutterMutation m = Radamsa::TreeStutterMutation(10, {}, randy);

	std::string out = m.mutate(input);
	REQUIRE(out == "spaghetti sauce");
	REQUIRE(randy.allExpectedCallsMade());
}

TEST_CASE("Some options") {
	std::string input = "spa(g{h}e)tt<(i[ ]sau)ce";
	MockRandom randy = MockRandom();
	Radamsa::TreeStutterMutation m = Radamsa::TreeStutterMutation(10, {}, randy);
	randy.expectedNext(0);
	randy.expectedNext(23);

	std::string out = m.mutate(input);
	REQUIRE(out == "spa(g{h}e)tt<(i(i(i(i(i(i(i(i(i(i(i(i(i(i(i(i(i(i(i(i(i(i(i(i[ ]sau)sau)sau)sau)sau)sau)sau)sau)sau)sau)sau)sau)sau)sau)sau)sau)sau)sau)sau)sau)sau)sau)sau)sau)ce");
	REQUIRE(randy.allExpectedCallsMade());
}
