#include "catch.hpp"
#include "mutation/tree/treeUtils.h"
#include <string>

TEST_CASE("Basic Tree Parse + Flatten") {
	std::string in = "(((potato)))";
	Radamsa::TreeUtils utils = Radamsa::TreeUtils();
	std::map<std::string, std::string> out;

	SECTION("No Mutation") {
		out = utils.sublists(in);

		REQUIRE(in == "#0002#");
		REQUIRE(out.size() == 3);

		utils.flatten(in, out);

		REQUIRE(in == "(((potato)))");
	}

	SECTION("Some Mutation") {
		out = utils.sublists(in);

		REQUIRE(in == "#0002#");
		REQUIRE(out.size() == 3);

		out["#0000#"] = "(potato)(potato)(potato)";
		utils.flatten(in, out);

		REQUIRE(in == "(((potato)(potato)(potato)))");
	}
}