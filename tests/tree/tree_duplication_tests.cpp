#include "catch.hpp"
#include "../mock/mock_random.h"
#include "mutation/tree/treeDuplicateMutation.h"

TEST_CASE("No sublists duped") {
	std::string input = "spaghetti sauce";
	MockRandom randy = MockRandom();
	Radamsa::TreeDuplicateMutation m = Radamsa::TreeDuplicateMutation(10, {}, randy);

	std::string out = m.mutate(input);
	REQUIRE(out == input);
	REQUIRE(randy.allExpectedCallsMade());
}

TEST_CASE("One sublist duped") {
	std::string input = "spaghetti (marinara) sauce";
	MockRandom randy = MockRandom();
	Radamsa::TreeDuplicateMutation m = Radamsa::TreeDuplicateMutation(10, {}, randy);
	randy.expectedNext(0);

	std::string out = m.mutate(input);
	REQUIRE(out == "spaghetti (marinara)(marinara) sauce");
	REQUIRE(randy.allExpectedCallsMade());
}

TEST_CASE("Nested sublists duped") {
	std::string input = "spaghetti (mari<nara>) sauce";
	MockRandom randy = MockRandom();
	Radamsa::TreeDuplicateMutation m = Radamsa::TreeDuplicateMutation(10, {}, randy);
	randy.expectedNext(0);
	randy.expectedNext(1);

	std::string out = m.mutate(input);
	REQUIRE(out == "spaghetti (mari<nara><nara>) sauce");

	out = m.mutate(input);
	REQUIRE(out == "spaghetti (mari<nara>)(mari<nara>) sauce");
	REQUIRE(randy.allExpectedCallsMade());
}

TEST_CASE("Random sublist duped") {
	std::string input = "((<{}>)[]\"\"\'\')";
	MockRandom randy = MockRandom();
	Radamsa::TreeDuplicateMutation m = Radamsa::TreeDuplicateMutation(10, {}, randy);
	randy.expectedNext(0);
	randy.expectedNext(5);

	std::string out = m.mutate(input);
	REQUIRE(out == "((<{}>)[]\"\"\'\'\'\')");

	out = m.mutate(input);
	REQUIRE(out == "((<{}>)(<{}>)[]\"\"\'\')");
	REQUIRE(randy.allExpectedCallsMade());
}