#include "catch.hpp"
#include "iostream"
#include "mutation/byte/bytePermuteMutation.h"
#include "../mock/mock_random.h"

// echo "abcde" | radamsa -p od -m bp -s seed#    to run Radamsa and test

TEST_CASE("Test construction of Byte Permute Mutation") {

	struct radamsa_constants constants = { 0 };
	Radamsa::Random random(0);
	Radamsa::BytePermuteMutation m{ 5, constants, random };

	REQUIRE(m.score == 0);
	REQUIRE(m.priority == 5);
	REQUIRE(m.getName() == "bp");
}

TEST_CASE("Test Byte Permute Mutation with empty input string") {
	struct radamsa_constants constants = { 0 };

	SECTION("Random seed 0") {
		std::string input = "";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(1);
		mock.expectedNext(9);
		Radamsa::Random* random = &mock;
		Radamsa::BytePermuteMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output.length() == 0);
		REQUIRE(output == "");
	}
}

TEST_CASE("Test Byte Permute Mutation with input length of 5") {

	struct radamsa_constants constants = { 0 };

	SECTION("Random seed 33") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(1);
		mock.expectedNext(1);
		mock.expectedNext(6);
		Radamsa::Random* random = &mock;
		Radamsa::BytePermuteMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "abdec"); // was acebd
	}

	SECTION("Random seed 111") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(1);
		mock.expectedNext(-1);
		mock.expectedNext(8);
		Radamsa::Random* random = &mock;
		Radamsa::BytePermuteMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "abdec"); // was acebd
	}
}
//
//TEST_CASE("Test Byte Permute Mutation with input length of 10") {
//
//	struct radamsa_constants constants = { 0 };
//
//	SECTION("Random seed 456") {
//		std::string input = "abcde12345";
//		MockRandom mock;
//		mock.expectedNext(10);
//		Radamsa::Random* random = &mock;
//		Radamsa::BytePermuteMutation m{ 5, constants, *random };
//		std::string output = m.mutate(input);
//		REQUIRE(output == "13acb24e5d");
//	}
//
//	SECTION("Random seed 3722") {
//		std::string input = "abcde12345";
//		MockRandom mock;
//		mock.expectedNext(18);
//		Radamsa::Random* random = &mock;
//		Radamsa::BytePermuteMutation m{ 5, constants, *random };
//		std::string output = m.mutate(input);
//		REQUIRE(output == "abcd312e45");
//	}
//
//	SECTION("Random seed 7788") {
//		std::string input = "abcde12345";
//		MockRandom mock;
//		mock.expectedNext(6);
//		Radamsa::Random* random = &mock;
//		Radamsa::BytePermuteMutation m{ 5, constants, *random };
//		std::string output = m.mutate(input);
//		REQUIRE(output == "abcde15423");
//	}
//}