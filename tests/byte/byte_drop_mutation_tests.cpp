#include "catch.hpp"
#include "iostream"
#include "mutation/byte/byteDropMutation.h"
#include "../mock/mock_random.h"

// echo "abcde" | radamsa -p od -m bd -s seed#    to run Radamsa and test


TEST_CASE("Test construction of Byte Drop Mutation") {

	struct radamsa_constants constants = { 0 };
	Radamsa::Random random(0);
	Radamsa::ByteDropMutation m{ 5, constants, random };

	REQUIRE(m.score == 0);
	REQUIRE(m.priority == 5);
	REQUIRE(m.getName() == "bd");
}

TEST_CASE("Test Byte Drop Mutation with empty input string") {
	struct radamsa_constants constants = { 0 };

	SECTION("Input Empty String") {
		std::string input = "";
		
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(-1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDropMutation m{ 5, constants, *random };
		
		std::string output = m.mutate(input);
		REQUIRE(output == "");
	}
}

TEST_CASE("Test Byte Drop Mutation with input length of 1") {
	struct radamsa_constants constants = { 0 };

	SECTION("Random seed 0") {
		std::string input = "a";

		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(-1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDropMutation m{ 5, constants, *random };

		std::string output = m.mutate(input);
		REQUIRE(input.length() == 1);
		REQUIRE(output.length() == 0);
		REQUIRE(output == "");
	}

	SECTION("Random seed 1000") {
		std::string input = "a";
		MockRandom mock;
		mock.expectedNext(1);
		mock.expectedNext(-1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDropMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(input.length() == 1);
		REQUIRE(output.length() == 1);
		REQUIRE(output == "a");
	}

	SECTION("Random seed 3000000") {
		std::string input = "k";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDropMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(input.length() == 1);
		REQUIRE(output.length() == 0);
		REQUIRE(output == "");
	}
}

TEST_CASE("Test Byte Drop Mutation with input length of 5") {

	struct radamsa_constants constants = { 0 };

	SECTION("Random seed 0") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(2);
		mock.expectedNext(-1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDropMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		//std::cout << output;
		REQUIRE(input.length() == 5);
		REQUIRE(output.length() == 4);
		REQUIRE(output == "abde");
	}

	SECTION("Random seed 2") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(-1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDropMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		//std::cout << output;
		REQUIRE(input.length() == 5);
		REQUIRE(output.length() == 4);
		REQUIRE(output == "bcde");
	}

	SECTION("Random seed 10") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(2);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDropMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		//std::cout << output;
		REQUIRE(input.length() == 5);
		REQUIRE(output.length() == 4);
		REQUIRE(output == "abde");
	}

	SECTION("Random seed 277") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(3);
		mock.expectedNext(-1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDropMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		//std::cout << output;
		REQUIRE(input.length() == 5);
		REQUIRE(output.length() == 4);
		REQUIRE(output == "abce");
	}

	SECTION("Random seed 300") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(2);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDropMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		//std::cout << output;
		REQUIRE(input.length() == 5);
		REQUIRE(output.length() == 4);
		REQUIRE(output == "abde");
	}

	SECTION("input contain z and seed 8000") {
		std::string input = "abzzz";
		MockRandom mock;
		mock.expectedNext(2);
		mock.expectedNext(-1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDropMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		//std::cout << output;
		REQUIRE(input.length() == 5);
		REQUIRE(output.length() == 4);
		REQUIRE(output == "abzz");
	}

	SECTION("input contain z and seed 2345183") {
		std::string input = "abzzz";
		MockRandom mock;
		mock.expectedNext(1);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDropMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		//std::cout << output;
		REQUIRE(input.length() == 5);
		REQUIRE(output.length() == 4);
		REQUIRE(output == "azzz");
	}

	SECTION("input contain A and seed 3000000") {
		std::string input = "abAAA";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteDropMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		//std::cout << output;
		REQUIRE(input.length() == 5);
		REQUIRE(output.length() == 4);
		REQUIRE(output == "bAAA");
	}
}