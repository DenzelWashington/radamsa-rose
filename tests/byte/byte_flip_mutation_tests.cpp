#include "catch.hpp"
#include "iostream"
#include "mutation/byte/byteFlipMutation.h"
#include "../mock/mock_random.h"

// echo "abcde" | radamsa -p od -m bf -s seed#    to run Radamsa and test

TEST_CASE("Test construction of Byte Flip Mutation") {

	struct radamsa_constants constants = { 0 };
	Radamsa::Random random(0);
	Radamsa::ByteFlipMutation m{ 5, constants, random };

	REQUIRE(m.score == 0);
	REQUIRE(m.priority == 5);
	REQUIRE(m.getName() == "bf");
}

TEST_CASE("Test Byte Flip Mutation with empty input string") {
	struct radamsa_constants constants = { 0 };

	SECTION("Input Empty String") {
		std::string input = "";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(2);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteFlipMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "");
	}
}

TEST_CASE("Test Byte Flip Mutation with input length of 5") {

	struct radamsa_constants constants = { 0 };

	SECTION("Random seed 0") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(2);
		mock.expectedNext(2);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteFlipMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "abgde");
	}

	SECTION("Random seed 198") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(2);
		mock.expectedNext(0);
		mock.expectedNext(-1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteFlipMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "abbde");
	}

	SECTION("Random seed 315") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(3);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteFlipMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "ibcde");
	}

	SECTION("input contain z and seed 2020") {
		std::string input = "abzzz";
		MockRandom mock;
		mock.expectedNext(2);
		mock.expectedNext(4);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteFlipMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "abjzz");
	}

	SECTION("input contain A and seed 1980") {
		std::string input = "abAAA";
		MockRandom mock;
		mock.expectedNext(2);
		mock.expectedNext(4);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteFlipMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		//std::cout << output;
		REQUIRE(output == "abQAA");
	}

	SECTION("input contain B and seed 39393939") {
		std::string input = "abBBc";
		MockRandom mock;
		mock.expectedNext(3);
		mock.expectedNext(1);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteFlipMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "abB@c");
	}
}