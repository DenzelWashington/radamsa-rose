﻿#include "catch.hpp"
#include "iostream"
#include "mutation/byte/byteIncreaseMutation.h"
#include "../mock/mock_random.h"

// echo "abcde" | radamsa -p od -m bei -s seed#    to run Radamsa and test

TEST_CASE("Test construction of Byte Increase Mutation") {

	struct radamsa_constants constants = { 0 };
	Radamsa::Random random(0);
	Radamsa::ByteIncreaseMutation m{ 5, constants, random };

	REQUIRE(m.score == 0);
	REQUIRE(m.priority == 5);
	REQUIRE(m.getName() == "bei");
}

TEST_CASE("Test Byte Increase Mutation with empty input string") {
	struct radamsa_constants constants = { 0 };

	SECTION("Input Empty String") {
		std::string input = "";
		MockRandom mock;
		mock.expectedNext(0);
		mock.expectedNext(-1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteIncreaseMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "");
	}
}

TEST_CASE("Test Byte Increase Mutation with input length of 5") {

	struct radamsa_constants constants = { 0 };

	SECTION("Random seed 0") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(2);
		mock.expectedNext(-1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteIncreaseMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "abdde");

	}

	SECTION("Random seed 10") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(2);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteIncreaseMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "abdde");
	}

	SECTION("Random seed 300") {
		std::string input = "abcde";
		MockRandom mock;
		mock.expectedNext(2);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteIncreaseMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "abdde");
	}

	SECTION("input contain z and seed 778") {
		std::string input = "abzzz";
		MockRandom mock;
		mock.expectedNext(4);
		mock.expectedNext(-1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteIncreaseMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "abzz{");
	}

	SECTION("input contain A and seed 567") {
		std::string input = "abAAA";
		MockRandom mock;
		mock.expectedNext(4);
		mock.expectedNext(1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteIncreaseMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "abAAB");
	}

	SECTION("input contain B and seed 390") {
		std::string input = "abZZZ";
		MockRandom mock;
		mock.expectedNext(1);
		mock.expectedNext(-1);
		Radamsa::Random* random = &mock;
		Radamsa::ByteIncreaseMutation m{ 5, constants, *random };
		std::string output = m.mutate(input);
		REQUIRE(output == "acZZZ");
	}
}