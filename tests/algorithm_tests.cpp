#include "catch.hpp"
#include "radamsa-rose/constants.h"
#include "algorithm.h"

class ChunkSplitGenerator : public Radamsa::Generator {
public:
	int calls;
	ChunkSplitGenerator() : Generator(0, "Split Generator") {
		this->calls = 0;
	}

	std::deque<std::string> generate(std::string input) {
		int chunk = input.size() / 2;
		std::deque<std::string> out;
		out.push_back(input.substr(0, chunk));
		out.push_back(input.substr(chunk, input.length() - chunk));
		this->calls++;
		return out;
	}
};

TEST_CASE("Test Chunk split generator splits properly") {
	ChunkSplitGenerator gen;

	SECTION("Split even input") {
		std::deque<std::string> chunks = gen.generate("ab");
		REQUIRE(chunks.size() == 2);
		REQUIRE(chunks.front() == "a");
		REQUIRE(chunks.back() == "b");
	}

	std::deque<std::string> chunks = gen.generate("abc");
	REQUIRE(chunks.size() == 2);
	REQUIRE(chunks.front() == "a");
	REQUIRE(chunks.back() == "bc");
}

class ReverseChunksPattern : public Radamsa::Pattern {
public:
	int calls;
	ReverseChunksPattern(Radamsa::Random rand) : Pattern(0, radamsa_constants { 0 }, rand, "Null Pattern") {
		this->calls = 0;
	}

	std::deque<std::string> run(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) {
		this->calls++;
		inputChunks.push_back(inputChunks.front());
		inputChunks.pop_front();
		return inputChunks;
	}

	std::deque<std::string> applyPattern(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) override {
		return inputChunks;
	}
};

TEST_CASE("Test ReverseChunksPattern reverses queue") {
	ReverseChunksPattern pat(Radamsa::Random(0));
	std::deque<std::string> in;
	in.push_back("a");
	in.push_back("b");
	REQUIRE(in.front() == "a");
	REQUIRE(in.back() == "b");
	
	std::deque<std::string> out = pat.run(in, NULL);
	REQUIRE(out.front() == "b");
	REQUIRE(out.back() == "a");
}

TEST_CASE("Test algorithm calls pattern and generator") {
	ChunkSplitGenerator gen;
	ReverseChunksPattern pat(Radamsa::Random(0));

	Radamsa::AlgorithmParameters params{ NULL, &gen, &pat };
	std::string out = Radamsa::runAlgorithm("", params);

	REQUIRE(gen.calls == 1);
	REQUIRE(pat.calls == 1);
}

TEST_CASE("Test runAlgorithm puts input back together after chunk") {
	ChunkSplitGenerator gen;
	ReverseChunksPattern pat(Radamsa::Random(0));

	Radamsa::AlgorithmParameters params{ NULL, &gen, &pat };
	std::string out = Radamsa::runAlgorithm("abcd", params);
	REQUIRE(out == "cdab");
}

class DropChunksPattern : public Radamsa::Pattern {
public:
	int calls;
	DropChunksPattern(Radamsa::Random rand) : Pattern(0, radamsa_constants{ 0 }, rand, "Drop Pattern") {
		this->calls = 0;
	}
	std::deque<std::string> run(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) override {
		this->calls++;
		inputChunks.pop_front();
		return inputChunks;
	}

	std::deque<std::string> applyPattern(std::deque<std::string> inputChunks, Radamsa::Mutation* mutation) override {
		return inputChunks;
	}
};

TEST_CASE("Test drop chunks pattern") {
	DropChunksPattern pat(Radamsa::Random(0));
	std::deque<std::string> in;
	in.push_back("a");
	in.push_back("b");
	REQUIRE(in.front() == "a");
	REQUIRE(in.back() == "b");

	std::deque<std::string> out = pat.run(in, NULL);
	REQUIRE(out.front() == "b");
	REQUIRE(out.size() == std::size_t(1));
}

TEST_CASE("Test runAlgorithm uses only the chunks returned by pat") {
	ChunkSplitGenerator gen;
	DropChunksPattern pat(Radamsa::Random(0));

	Radamsa::AlgorithmParameters params{ NULL, &gen, &pat };
	std::string out = Radamsa::runAlgorithm("abcde", params);
	REQUIRE(out == "cde");
}

class PushNullByteMutation : public Radamsa::Mutation {
public:
	int calls;
	PushNullByteMutation() : Mutation(0, 0, "Push Null Byte") {
		this->calls = 0;
	}
	std::string mutate(std::string input) {
		input.insert(0, "\x00");
		return input;
	}
	int nextDelta() {
		return 0;
	}
};

TEST_CASE("Test null pointers passed into runAlgorithm") {
	ChunkSplitGenerator gen;
	DropChunksPattern pat(Radamsa::Random(0));
	PushNullByteMutation muta;

	SECTION("Null mutation passed in") {
		Radamsa::AlgorithmParameters params{ NULL, &gen, &pat };
		REQUIRE_NOTHROW(Radamsa::runAlgorithm("abcde", params));
	}

	SECTION("Null generator passed in") {
		Radamsa::AlgorithmParameters params{ &muta, NULL, &pat };
		REQUIRE_THROWS(Radamsa::runAlgorithm("abcde", params));
	}

	SECTION("Null pattern passed in") {
		Radamsa::AlgorithmParameters params{ &muta, &gen, NULL };
		REQUIRE_THROWS(Radamsa::runAlgorithm("abcde", params));
	}
}