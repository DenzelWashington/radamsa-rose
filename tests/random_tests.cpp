﻿#include "catch.hpp"
#include "random/random.h"
#include "random/bignum.h"
#include "radamsa-rose/constants.h"

// ALL magic numbers being tested against were manually calculated using owl lisp
TEST_CASE("walk") {
	Radamsa::Random rand = Radamsa::Random(0);

	SECTION("Seed 0 initialization") {
		Radamsa::Bignum out = Radamsa::Bignum();

		std::vector<uint8_t> bits = std::vector<uint8_t>();
		for (uint8_t bit : Radamsa::Random::promoter) bits.push_back(bit);
		std::reverse(bits.begin(), bits.end()); // Convert to [lsd ... msd]
		Radamsa::Bignum in = Radamsa::Bignum(bits);

		const uint8_t expectedbits[96] = { 1,0,0,0,1,0,0,0,1,1,1,0,1,0,0,0,1,1,1,1,1,0,0,0,1,1,0,1,0,1,0,1,1,1,0,1,1,0,0,0,1,1,0,0,0,0,1,0,0,1,1,0,1,1,0,1,0,1,1,0,0,1,1,1,1,0,1,1,0,1,0,1,0,1,1,1,0,0,0,0,0,1,1,1,1,1,1,0,0,0,1,0,1,1,0,0 };
		std::vector<uint8_t> expectedVec = std::vector<uint8_t>();
		for (uint8_t bit : expectedbits) expectedVec.push_back(bit);
		std::reverse(expectedVec.begin(), expectedVec.end()); // Convert to [lsd ... msd]
		Radamsa::Bignum expected = Radamsa::Bignum(expectedVec);

		rand.walk(&in, &out, rand.accumulator);
		bool check = (out == expected);
		REQUIRE(check);
	}

	SECTION("Seed 1337 initialization") {
		
		Radamsa::Bignum out = Radamsa::Bignum();
		Radamsa::Bignum seed = Radamsa::Bignum(1337);

		seed = seed + Radamsa::Bignum(1);

		std::vector<uint8_t> bits = std::vector<uint8_t>();
		for (uint8_t bit : Radamsa::Random::promoter) bits.push_back(bit);
		std::reverse(bits.begin(), bits.end()); // Convert to [lsd ... msd]
		Radamsa::Bignum in = Radamsa::Bignum(bits);

		in = seed * in;

		const uint8_t expectedbits[93] = { 1,0,0,0,1,0,1,1,1,0,0,0,1,0,0,1,0,1,1,1,1,1,0,1,0,1,1,1,0,1,1,1,0,0,1,1,0,0,0,1,0,0,0,1,0,1,1,0,1,0,0,0,0,0,0,0,0,1,1,0,0,0,1,0,1,1,1,1,1,1,1,1,1,0,0,1,1,0,1,1,1,0,1,0,0,0,0,1,1,0,0,1,1 };
		std::vector<uint8_t> expectedVec = std::vector<uint8_t>();
		for (uint8_t bit : expectedbits) expectedVec.push_back(bit);
		std::reverse(expectedVec.begin(), expectedVec.end()); // Convert to [lsd ... msd]
		Radamsa::Bignum expected = Radamsa::Bignum(expectedVec);

		rand.walk(&in, &out, rand.accumulator);

		bool check = (out == expected);
		REQUIRE(check);
	}

	SECTION("Seed 1234567") {
		
		Radamsa::Bignum out = Radamsa::Bignum();

		std::vector<uint8_t> bits = std::vector<uint8_t>();
		for (uint8_t bit : Radamsa::Random::promoter) bits.push_back(bit);
		std::reverse(bits.begin(), bits.end()); // Convert to [lsd ... msd]
		Radamsa::Bignum in = Radamsa::Bignum(bits);

		in = in * Radamsa::Bignum(1234568); // 13717422222222222222222085048

		const uint8_t expectedbits[96] = { 1,1,0,0,0,0,0,1,1,0,0,0,1,0,0,1,0,0,0,0,0,0,0,1,0,1,1,1,0,0,0,1,1,1,1,1,1,0,1,0,1,1,1,0,1,0,0,0,1,0,0,1,1,0,1,0,0,0,1,0,0,0,1,0,1,0,0,1,1,0,1,0,0,1,0,1,1,0,1,1,1,1,0,1,1,1,0,0,0,1,0,1,1,0,0,1 };
		std::vector<uint8_t> expectedVec = std::vector<uint8_t>();
		for (uint8_t bit : expectedbits) expectedVec.push_back(bit);
		std::reverse(expectedVec.begin(), expectedVec.end()); // Convert to [lsd ... msd]
		Radamsa::Bignum expected = Radamsa::Bignum(expectedVec);

		rand.walk(&in, &out, rand.accumulator);

		bool check = (out == expected);
		REQUIRE(check);
	}
}

TEST_CASE("Test intialization values") {
	unsigned long long int next;
	SECTION("Seed = 0") {
		Radamsa::Random random = Radamsa::Random(0);
		next = random.stream_a.peek();
		// 4,019,730,731,745,956,117,329
		// stream_a should be 110110011110100011111000 | 010110110001001110100110 | 101111001011101101010001
		REQUIRE(next == 7372332);

		next = random.nextInteger(100);
		REQUIRE(next == 43);
	}

	SECTION("Seed = 1") {
		Radamsa::Random random = Radamsa::Random(1);
		REQUIRE(random.nextInteger(100) == 87);
	}

	SECTION("Seed = 23232323") {
		Radamsa::Random random = Radamsa::Random(23232323);
		REQUIRE(random.nextInteger(100) == 25);
	}

	SECTION("Seed = 2^96") { // 79228162514264337593543950336
		std::vector<uint8_t> bits = std::vector<uint8_t>();
		for (int i = 0; i < 96; i++) bits.push_back(0);
		bits.push_back(1);
		Radamsa::Bignum num = Radamsa::Bignum(bits);

		Radamsa::Random random = Radamsa::Random(num);
		uint32_t val = random.nextInteger(10000);
		REQUIRE(val == 1867);
	}

	SECTION("Seed = 1337") {
		Radamsa::Random random = Radamsa::Random(1337);
		uint32_t val = random.stream_a.peek();
		REQUIRE(val == 15954995);
	}
}

TEST_CASE("Test next int") {
	SECTION("s0 d100 d100") {
		Radamsa::Random random = Radamsa::Random(0);
		uint32_t next = random.nextInteger(100);
		REQUIRE(next == 43);
		next = random.nextInteger(100);
		REQUIRE(next == 88);
	}
	
	// > (lets ((rs (seed->rands 0)))
	//		 (let loop ((rs rs) (n 10))
	//			(cond
	//				((eq ? n 0) (rand rs 100))
	//		  		(else 
	//					(lets ((rs _ (rand rs 100))) 
	//						(loop rs (- n 1)))))))
	//
	// > '(values #function 7)
	SECTION("Seed 0, 11 generations") {
		Radamsa::Random random = Radamsa::Random(0);
		int out;
		for (int i = 10; i >= 0; i--) {
			out = random.nextInteger(100);
		}
		bool check = out == 7;
		REQUIRE(check);
	}

	//SECTION("Seed 0, 100,001 generations") {
	//	Radamsa::Random random = Radamsa::Random(0);
	//	int out;
	//	for (int i = 100000; i >= 0; i--) {
	//		out = random.nextInteger(100);
	//	}
	//	REQUIRE(out == 74);
	//}

	//SECTION("Seed 23232323, 100,001 generations") {
	//	Radamsa::Random random = Radamsa::Random(23232323);
	//	int out;
	//	for (int i = 100000; i >= 0; i--) {
	//		out = random.nextInteger(100);
	//	}
	//	REQUIRE(out == 54);
	//}

	/*
	(lets ((rs(seed->rands 1337))) 
		(let loop((rs rs) (out `()) (n 20)) 
			(cond
				((eq ? n 0) out) 
				(else 
					(lets((rs next(rand rs 10000))) 
						(loop rs(cons next out) (-n 1)))))))
	
	'(4469 467 449 2937 5131 3294 2937 7546 5836 5604 5467 3404 8582 2231 1326 7903 8184 7527 7004 9509)
	*/
	SECTION("Full list") {
		Radamsa::Random random = Radamsa::Random(1337);

		int target[20] = { 4469, 467, 449, 2937, 5131, 3294, 2937, 7546, 5836, 5604, 5467, 3404, 8582, 2231, 1326, 7903, 8184, 7527, 7004, 9509 };

		for (int i = 0; i < 20; i++) { // target is backwards because cons works meanly
			uint32_t next = random.nextInteger(10000);
			bool check = next == target[19 - i];
			REQUIRE(check);
		}
	}

	SECTION("Bignum") {
		Radamsa::Random random = Radamsa::Random(1337);
		// 2^96 :: 79228162514264336593543950336
		std::vector<uint8_t> bits = std::vector<uint8_t>();
		for (int i = 0; i < 96; i++) bits.push_back(0);
		bits.push_back(1);
		Radamsa::Bignum num = Radamsa::Bignum(bits);

		Radamsa::Bignum big = random.nextInteger(num);
		
		// 64025777493979351874870146099 :: 0x D185E2 C0B3C9 B3502B F37433 :: fx 13731298 ‬12628937 11751467 ‬15954995
		std::vector<uint8_t> expectedBits = std::vector<uint8_t>();
		uint8_t expectedBinary[96] = { 1,1,0,1,0,0,0,1,1,0,0,0,0,1,0,1,1,1,1,0,0,0,1,0,1,1,0,0,0,0,0,0,1,0,1,1,0,0,1,1,1,1,0,0,1,0,0,1,1,0,1,1,0,0,1,1,0,1,0,1,0,0,0,0,0,0,1,0,1,0,1,1,1,1,1,1,0,0,1,1,0,1,1,1,0,1,0,0,0,0,1,1,0,0,1,1 };
		for (uint8_t bit : expectedBinary) expectedBits.push_back(bit);
		std::reverse(expectedBits.begin(), expectedBits.end()); // convert to [lsd...msd]
		Radamsa::Bignum expected = Radamsa::Bignum(expectedBits);
		
		bool check = (big == expected);
		REQUIRE(check);
	}
}

TEST_CASE("Does Occur") {
	struct rational prob;
	prob.numerator = 11;
	prob.denominator = 20;

	Radamsa::Random random = Radamsa::Random(3735928559); // 0xDEADBEEF
	bool target[20] = { false ,true ,true ,false ,true ,true ,true ,false ,true ,false ,false ,false ,false ,false ,false ,false ,true ,false ,false ,false };

	for (int i = 0; i < 20; i++) {
		bool check = random.doesOccur(prob) == target[19 - i];
		REQUIRE(check); // we go backwards because cons builds lists backwards
	}
}

TEST_CASE("Test random next integer is exclusive") {
	Radamsa::Random rand(0);
	REQUIRE(rand.nextInteger(1) == 0);
	REQUIRE(rand.nextInteger(1) == 0);

	rand = Radamsa::Random(232323);
	REQUIRE(rand.nextInteger(1) == 0);
	REQUIRE(rand.nextInteger(1) == 0);

	rand = Radamsa::Random(42);
	REQUIRE(rand.nextInteger(1) == 0);
	REQUIRE(rand.nextInteger(1) == 0);

	rand = Radamsa::Random(69);
	REQUIRE(rand.nextInteger(1) == 0);
	REQUIRE(rand.nextInteger(1) == 0);
}

// This also indirectly tests nextNbit since log uses that.
TEST_CASE("Test random log replicates radamsa behavior", "[random][log]") {
	// (lets ((rs (seed->rands 0)) (rs n (rand-log rs 10)) (rs n2 (rand-log rs 20))) (values n n2))
	SECTION("Seed 0") {
		Radamsa::Random rand(0);
		Radamsa::Bignum actual1 = rand.nextLogInteger(10);
		Radamsa::Bignum actual2 = rand.nextLogInteger(20);

		Radamsa::Bignum expected1 = Radamsa::Bignum(15);
		Radamsa::Bignum expected2 = Radamsa::Bignum(0);

		REQUIRE(actual1 == expected1);
		REQUIRE(actual2 == expected2);

		CHECK(rand.peekState() == 8986796);
	}
	// (lets ((rs (seed->rands 24)) (rs n (rand-log rs 30)) (rs n2 (rand-log rs 5))) (values n n2))
	SECTION("Seed 24") {
		Radamsa::Random rand(24);

		Radamsa::Bignum actual1 = rand.nextLogInteger(30);
		Radamsa::Bignum actual2 = rand.nextLogInteger(5);

		Radamsa::Bignum expected1 = Radamsa::Bignum(355024298);
		Radamsa::Bignum expected2 = Radamsa::Bignum(10);

		REQUIRE(actual1 == expected1);
		REQUIRE(actual2 == expected2);
	}
}

TEST_CASE("Test random nextRangeInt replicates radamsa behavior", "[random]") {
	/* 
	(lets ((rs (seed->rands 0))
		   (rs n (rand-range rs 10 15))
		   (rs n2 (rand-range rs 5 100))
		   (rs n3 (rand-range rs 24 25)))
		   (values n n2 n3))
	*/
	SECTION("Seed 0") {
		Radamsa::Random rand(0);
		REQUIRE(rand.nextIntegerRange(10, 15) == 12);
		REQUIRE(rand.nextIntegerRange(5, 100) == 89);
		REQUIRE(rand.nextIntegerRange(24, 25) == 24);
	}

	/*
	(lets ((rs (seed->rands 0))
		   (rs n (rand-range rs 1 10))
		   (rs n2 (rand-range rs 0 100))
		   (rs n3 (rand-range rs 1234 12345)))
		   (values n n2 n3))
	*/
	SECTION("Seed 25") {
		Radamsa::Random rand(25);
		REQUIRE(rand.nextIntegerRange(1, 10) == 4);
		REQUIRE(rand.nextIntegerRange(0, 100) == 68);
		REQUIRE(rand.nextIntegerRange(1234, 12345) == 7428);
	}
}

TEST_CASE("Test Random permutation shuffles a list of integers", "[random, permute]") {
	Radamsa::Random rand(0);

	SECTION("Empty list") {
		std::vector<int> integers;
		rand.permute(integers, 0, integers.size());
		CHECK(integers.empty());
	}

	SECTION("List of size 1") {
		std::vector<int> integers{ 1 };
		rand.permute(integers, 0, integers.size());
		REQUIRE(integers.size() == 1);
	}

	// (shuffle rs '(1 2))
	SECTION("List of size 2") {
		std::vector<int> integers{ 1, 2 };
		rand.permute(integers, 0, integers.size());
		REQUIRE(integers.size() == 2);
		REQUIRE(integers.front() == 2);
		REQUIRE(integers.back() == 1);
	}

	// (shuffle rs '(1 2 3))
	SECTION("List of size 3") {
		std::vector<int> integers{ 1, 2, 3 };
		rand.permute(integers, 0, integers.size());
		REQUIRE(integers.size() == 3);
		REQUIRE(integers.front() == 2);
		REQUIRE(integers[1] == 1);
		REQUIRE(integers.back() == 3);
	}
	
	SECTION("List of size 5") {
		std::vector<int> integers{ 1, 2, 3, 4, 5 };
		rand.permute(integers, 0, integers.size());
		REQUIRE(integers.size() == 5);
		REQUIRE(integers.front() == 2);
		REQUIRE(integers[1] == 4);
		REQUIRE(integers[2] == 1);
		REQUIRE(integers[3] == 5);
		REQUIRE(integers.back() == 3);
	}
}

TEST_CASE("Test Random permutation shuffles a list of integers exact to radamsa", "[random][permute]") {
	Radamsa::Random rand(0);

	SECTION("List size of 1") {
		std::vector<char> list{'a'};
		rand.permute(list, 0, list.size());
	
		REQUIRE(list.size() == 1);
		CHECK(rand.peekState() == 14922861);
	}

	SECTION("List size of 2") {
		std::vector<char> list{'a', 'b'};
		rand.permute(list, 0, list.size());
	
		CHECK(rand.peekState() == 597886);
		REQUIRE(list.front() == 'b');
		REQUIRE(list.back() == 'a');
	}

	SECTION("List size of 3") {
		std::vector<char> list{'a', 'b', 'c'};
		rand.permute(list, 0, list.size());

		CHECK(rand.peekState() == 8986796);
		REQUIRE(list.front() == 'b');
		REQUIRE(list.back() == 'c');
	}

	SECTION("List size of 5") {
		std::vector<char> list{'a', 'b', 'c', 'd', 'e'};
		rand.permute(list, 0, list.size());
	
		REQUIRE(list.front() == 'b');
		REQUIRE(list[1] == 'd');
		REQUIRE(list[2] == 'a');
		REQUIRE(list[3] == 'e');
		REQUIRE(list.back() == 'c');

		CHECK(rand.peekState() == 7008760);	
	}
}

/*
Test stack overflow exception with seed 100, list size 10
*/
TEST_CASE("Test Random permutation shuffles a list of that stack overflowed once", "[random, permute]") {
	Radamsa::Random rand(100);
	std::vector<int> integers{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	rand.permute(integers, 0, integers.size());

	REQUIRE(integers.size() == 10);
	REQUIRE(integers.front() == 5);
	REQUIRE(integers[1] == 3);
	REQUIRE(integers[4] == 9);
	REQUIRE(integers[8] == 2);
	REQUIRE(integers.back() == 6);
}

TEST_CASE("Test reservoir sample") {
	Radamsa::Random rand(100);
	std::vector<std::string> list{ "hi", "this", "is", "a", "list", "of", "words" };

	std::vector<std::string> out = rand.reservoirSample(list, 2);

	REQUIRE(rand.stream_a.peek() == 627863);

	REQUIRE(list.size() == 7);
	REQUIRE(out.size() == 2);
	REQUIRE(out[0] == "a");
	REQUIRE(out[1] == "is");

}

TEST_CASE("Test nextRangeInt bughunting", "[random]") {
	Radamsa::Random randy = Radamsa::Random(10);
	while (randy.peekState() != 8301652) randy.generateNext();

	int len = 2;
	int first = randy.nextIntegerRange(0, len - 1ULL);
	int second = randy.nextIntegerRange(first + 1ULL, len);

	REQUIRE(first == 0);
	REQUIRE(second == 1);
}

TEST_CASE("Test doesOccur bughunting") {
	Radamsa::Random pamelaRanderson(89);
	while (pamelaRanderson.peekState() != 13652682) pamelaRanderson.generateNext();
	rational test{ 4, 5 };
	bool check = pamelaRanderson.doesOccur(test);

	CHECK(pamelaRanderson.peekState() == 15976963);
	REQUIRE_FALSE(check);
}