#pragma once
#include "random/random.h"
#include "random/bignum.h"
#include <queue>

class MockRandom : public Radamsa::Random {
public:
	MockRandom();
	unsigned long long nextInteger(unsigned long long max) override;
	bool doesOccur(struct rational probability) override;
	unsigned int nextBlockSize(unsigned int minSize, unsigned int maxSize) override;
	int randomDelta() override;
	int randomDeltaUp(rational probability) override;
	Radamsa::Bignum nextNBit(unsigned int n);
	Radamsa::Bignum nextLogInteger(unsigned long long max);
	unsigned long long nextIntegerRange(unsigned long long low, unsigned long long high);
	bool allExpectedCallsMade();
	void expectedNext(unsigned long long nextNumber);
	std::vector<std::string> reservoirSample(std::vector<std::string> list, int n);
private:
	std::queue<unsigned long long> provider;
	int fromProvider();
};