#include "catch.hpp"
#include "generator/randomgenerator.h"
#include "algorithm.h"

TEST_CASE("Test RandomGenerator terminates and generates output", "[generator][.gen-random]") {
    struct radamsa_constants consts = { 0 };
    consts.maxBlockSize = 4096;
    Radamsa::Random random(0);
    Radamsa::RandomGenerator gen(1, consts, random);

    std::deque<std::string> inputStream = gen.generate("This shouldn't matter");
    REQUIRE_FALSE(inputStream.empty());
}

TEST_CASE("Test RandomGenerator generates correct radamsa output", "[generator][.gen-rand-verify]") {
    struct radamsa_constants consts = { 0 };
	consts.maxBlockSize = 4096;    // Max block size by Aki

    /// bin/radamsa -g random -s 0 -m bi -p od

    //  echo "" | bin/radamsa -m bi -g random -M - -s 0 > aaaaa.txt
    SECTION("Seed 0") {
        Radamsa::Random random(0); 
        random.generateNext(); // Simulate a mutation score random call
        Radamsa::RandomGenerator gen(0, consts, random);
		std::deque<std::string> output = gen.generate("");

        REQUIRE(random.peekState() == 7008760);
        REQUIRE(output.size() == 9);

        std::string firstBlock = output.front();
        REQUIRE((unsigned char)firstBlock.front() == 104); // Check first two bits in first block
        REQUIRE((unsigned char)firstBlock.at(1) == 213);

        REQUIRE((unsigned char)output.back().back() == 134); // Check final bit in last block
	}

    SECTION("Seed 123") {
        Radamsa::Random random(123); 
        random.generateNext(); // Simulate a mutation score random call
        Radamsa::RandomGenerator gen(0, consts, random);
        std::deque<std::string> output = gen.generate("");

        REQUIRE(random.peekState() == 11741470);
        REQUIRE(output.size() == 6);

        std::string firstBlock = output.front();
        REQUIRE((unsigned char)firstBlock.front() == 228); // Check first two bits in first block
        REQUIRE((unsigned char)firstBlock.at(1) == 23);

        REQUIRE((unsigned char)output.back().back() == 244); // Check final bit in last block
        REQUIRE(output.back().size() == 63);
    }

    SECTION("Seed 2323") {
        Radamsa::Random random(2323); 
        random.generateNext(); // Simulate a mutation score random call
        Radamsa::RandomGenerator gen(0, consts, random);
        std::deque<std::string> output = gen.generate("");

        REQUIRE(random.peekState() == 13546725);
        REQUIRE(output.size() == 34);

        std::string firstBlock = output.front();
        REQUIRE((unsigned char)firstBlock.front() == 255); // Check first two bits in first block
        REQUIRE((unsigned char)firstBlock.at(1) == 9);

        REQUIRE((unsigned char)output.back().back() == 126); // Check final bit in last block
        REQUIRE((unsigned char)output.back().front() == 212); // Check final bit in last block
    }
}

TEST_CASE("Test RandomGenerator does not error on negative block size", "[generator][.gen-random]") {
    struct radamsa_constants consts = { 0 };
    consts.maxBlockSize = -1;
    Radamsa::Random random(0);
    Radamsa::RandomGenerator gen(0, consts, random);
    REQUIRE_THROWS(gen.generate(""));
}