#include "catch.hpp"
#include "mock/mock_random.h"
#include "mutation/sequenceRepeatMutation.h"

using namespace Radamsa;

TEST_CASE("Sequence Repeat") {
	struct radamsa_constants constants = { 0 };
	MockRandom mock = MockRandom();
	SequenceRepeatMutation s = SequenceRepeatMutation(12, constants, mock);
	std::deque<std::string> in;
	in.push_back("spaghetti sauce");
	
	SECTION("sauce 1") {
		mock.expectedNext(3);
		mock.expectedNext(7);
		mock.expectedNext(14);
		mock.expectedNext(2);
		std::string expected = "spaghetghetghetghetghetghetghetghetghetghetghetghetghetghetti sauce";
		s.mutate(in);
		std::string actual = in.front() + in.back();
		REQUIRE(actual == "spati sauce");
		REQUIRE(in.size() == 16);
		REQUIRE(mock.allExpectedCallsMade());
	}

	SECTION("sauce 2") {
		mock.expectedNext(8);
		mock.expectedNext(10);
		mock.expectedNext(61);
		mock.expectedNext(2);
		std::string expected = "spaghetti i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i i sauce";
		s.mutate(in);
		std::string actual = in.front() + in.back();
		REQUIRE(actual == "spaghettsauce");
		REQUIRE(mock.allExpectedCallsMade());
	}

	SECTION("sauce 3") {
		mock.expectedNext(14);
		mock.expectedNext(15);
		mock.expectedNext(244);
		mock.expectedNext(2);
		std::string expected = "spaghetti sauceeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee";
		s.mutate(in);
		std::string actual = in.front() + in.back();
		REQUIRE(actual == "spaghetti sauc");
		REQUIRE(mock.allExpectedCallsMade());
	}

	SECTION("sauce 4") {
		mock.expectedNext(6);
		mock.expectedNext(13);
		mock.expectedNext(3);
		mock.expectedNext(2);
		std::string expected = "spaghece";
		s.mutate(in);
		std::string actual = in.front() + in.back();
		REQUIRE(actual == expected);
		REQUIRE(mock.allExpectedCallsMade());
	}

	SECTION("sauce 5") {
		mock.expectedNext(8);
		mock.expectedNext(14);
		mock.expectedNext(2);
		mock.expectedNext(2);
		std::string expected = "spaghette";
		s.mutate(in);
		std::string actual = in.front() + in.back();
		REQUIRE(actual == expected);
		REQUIRE(mock.allExpectedCallsMade());
	}

	SECTION("sauce 6") {
		mock.expectedNext(12);
		mock.expectedNext(14);
		mock.expectedNext(493);
		mock.expectedNext(2);
		std::string expected = "spaghetti sae";
		s.mutate(in);
		std::string actual = in.front() + in.back();
		REQUIRE(actual == expected);
		REQUIRE(mock.allExpectedCallsMade());
	}

	SECTION("sauce 7") {
		mock.expectedNext(8);
		mock.expectedNext(13);
		mock.expectedNext(203);
		mock.expectedNext(2);
		std::string expected = "spaghettce";
		s.mutate(in);
		std::string actual = in.front() + in.back();
		REQUIRE(actual == expected);
		REQUIRE(mock.allExpectedCallsMade());
	}
}