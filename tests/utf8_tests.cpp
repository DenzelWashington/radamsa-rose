﻿#include "catch.hpp"
#include "mutation/utf8WidenMutation.h"
#include "mutation/utf8InsertMutation.h"
#include "mock/mock_random.h"

#include <locale>
#include <codecvt>

using namespace Radamsa;

// TEST_CASE("Check funny-unicode") {
// 	Utf8Utils utils = Utf8Utils();
// 	utils.generateFunnyUnicode();

// 	REQUIRE(utils.funnyUnicode.size() == 197);
// }

TEST_CASE("Widen Mutations", "[utf-widen]") {
	MockRandom randy = MockRandom();
	utf8WidenMutation m = utf8WidenMutation(0, { 0 }, randy);

	std::string in = "this is a string to be widened";
	std::string out = "";
	std::string expected = "";

	SECTION("Case 1") {
		randy.expectedNext(28);
		randy.expectedNext(28);

		expected = "this is a string to be widened";
	}

	SECTION("Case 2") {
		randy.expectedNext(7);
		randy.expectedNext(28);

		expected = "this is" + std::string({ (char) -64, (char) (' ' | 0b10000000)}) + "a string to be widened";
	}

	SECTION("Case 3") {
		randy.expectedNext(23);
		randy.expectedNext(28);

		expected = "this is a string to be widened";
	}


	out = m.mutate(in);

	REQUIRE(out == expected);
	REQUIRE(randy.allExpectedCallsMade());
}

TEST_CASE("Test utf8-insert mutations on a real random","[utf8-insert][utf8-real]") {
	Random randy(1337);
	utf8InsertMutation muta(0, { 0 }, randy);
	while (randy.peekState() != 10288840) randy.generateNext();

	std::string result = muta.mutate("Rias made me do this\n");
	CHECK(randy.peekState() == 5679373);
	REQUIRE(result.size() == 25);
	REQUIRE(result.substr(0, 12) == "Rias made me");
	CHECK((unsigned char) result[12] == 0xf3);
	CHECK((unsigned char) result[13] == 0xa0);
	CHECK((unsigned char) result[14] == 0x81);
	CHECK((unsigned char) result[15] == 0x82);
	REQUIRE(result.substr(16, 9) == " do this\n");

	REQUIRE(randy.peekState() == 5679373);
}