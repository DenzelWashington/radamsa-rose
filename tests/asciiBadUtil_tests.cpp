#include "catch.hpp"
#include "mutation/asciiBadUtils.h"
#include <string>
#include <vector>

using namespace Radamsa;

TEST_CASE("String parsing") {
	struct radamsa_constants constants{0};
	constants.minTexty = 6;
	
	std::string in = "And I said \"What the f Janice ? \" because she said she was \'having a medical emergency\' or \'something.";
	AsciiBadUtils utils = AsciiBadUtils(constants);

	AsciiNode node1{ "text", "And I said " };
	AsciiNode node2{ "delimited", "\"What the f Janice ? \"" };
	AsciiNode node3{ "text", " because she said she was " };
	AsciiNode node4{ "delimited", "'having a medical emergency'" };
	AsciiNode node5{ "text", " or 'something." };
	
	std::vector<AsciiNode> expectedChunks = { node1, node2, node3, node4, node5 };

	std::vector<AsciiNode> actualChunks = utils.stringLex(in);
	 
	bool check = true;
	for (int i = 0; i < expectedChunks.size(); i++) {
		if (expectedChunks.size() != actualChunks.size()) {
			check = false;
			break;
		}
		check = check && (expectedChunks[i] == actualChunks[i]);
	}

	REQUIRE(check);

	std::string unlexed = utils.stringUnlex(actualChunks);

	REQUIRE(in == unlexed);
}