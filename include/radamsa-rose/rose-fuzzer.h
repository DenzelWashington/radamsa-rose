#pragma once
#include "radamsa-rose_export.h"
#include "radamsa_definitions.h"
#include "constants.h"
#include <stddef.h>
#include <stdbool.h>

typedef void* radamsa_rose; 
RADAMSA_ROSE_EXPORT radamsa_rose newRadamsaRoseFuzzer(int randomSeed, const struct radamsa_constants constants);
RADAMSA_ROSE_EXPORT void deleteRadamsaRoseFuzzer(radamsa_rose radamsa);

RADAMSA_ROSE_EXPORT size_t runRadamsa(radamsa_rose radamsa, const char* input, size_t inputSize, char* target, size_t max_size);
RADAMSA_ROSE_EXPORT void initDefaultConstants(struct radamsa_constants* constants);

RADAMSA_ROSE_EXPORT void enableMutation(radamsa_rose radamsa, char* name);
RADAMSA_ROSE_EXPORT void disableMutation(radamsa_rose radamsa, char* name);
RADAMSA_ROSE_EXPORT void enableAllMutations(radamsa_rose radamsa); 
RADAMSA_ROSE_EXPORT void disableAllMutations(radamsa_rose radamsa);
RADAMSA_ROSE_EXPORT void setMutationPriority(radamsa_rose radamsa, char* name, int priority);

RADAMSA_ROSE_EXPORT void enablePattern(radamsa_rose radamsa, char* name);
RADAMSA_ROSE_EXPORT void disablePattern(radamsa_rose radamsa, char* name);
RADAMSA_ROSE_EXPORT void enableAllPatterns(radamsa_rose radamsa); 
RADAMSA_ROSE_EXPORT void disableAllPatterns(radamsa_rose radamsa);
RADAMSA_ROSE_EXPORT void setPatternPriority(radamsa_rose radamsa, char* name, int priority);

RADAMSA_ROSE_EXPORT void enableGenerator(radamsa_rose radamsa, char* name);
RADAMSA_ROSE_EXPORT void disableGenerator(radamsa_rose radamsa, char* name);
RADAMSA_ROSE_EXPORT void enableAllGenerators(radamsa_rose radamsa); 
RADAMSA_ROSE_EXPORT void disableAllGenerators(radamsa_rose radamsa);
RADAMSA_ROSE_EXPORT void setGeneratorPriority(radamsa_rose radamsa, char* name, int priority);

RADAMSA_ROSE_EXPORT void addNewMutation(radamsa_rose radamsa, int (*mutate)(struct input_chunk*, struct radamsa_constants, radamsa_random), char* name, int priority);
RADAMSA_ROSE_EXPORT void addNewPattern(radamsa_rose radamsa, struct input_chunk* (*runFunc)(struct input_chunk*, radamsa_mutation, struct radamsa_constants, radamsa_random), char* name, int priority);
RADAMSA_ROSE_EXPORT void addNewGenerator(radamsa_rose radamsa, struct input_chunk* (*genFunc)(const char*, size_t size, struct radamsa_constants, radamsa_random), char* name, int priority);