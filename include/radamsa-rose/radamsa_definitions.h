#pragma once

// Define random and functions for customization
typedef void* radamsa_random;

// Define mutation for patterns to use
typedef void* radamsa_mutation;

// Define chunk that mutations, patterns, and generators use
struct input_chunk {
    int chunkSize;
    char* chunkPointer;
    struct input_chunk* next;
};